unit Funcoes;

interface

uses
    Vcl.Dialogs, Vcl.Controls, Grids, DBGrids, System.SysUtils;

function confirma(pergunta: string): boolean;
function emailValido(email: string): boolean;
procedure alerta(mensagem: string);
procedure mensagem(mensagem: string);
procedure erro(mensagem: string);
function procuraColuna(grid: tdbgrid; campo: string): Integer;

implementation

function confirma(pergunta: string): boolean;
begin
    Result := MessageDlg(pergunta, mtConfirmation, [mbYes, mbNo], 0) = mrYes;
end;

function emailValido(email: string): boolean;
begin
    if (Pos(' ', email) > 0) or (Pos('.', email) = 0) or (Pos('@', email) = 0) then begin
        result := False;
    end else begin
        result := True;
    end;
end;

procedure alerta(mensagem: string);
begin
    MessageDlg(mensagem, mtWarning, [mbOk], 0);
end;

procedure mensagem(mensagem: string);
begin
    MessageDlg(mensagem, mtInformation, [mbOk], 0);
end;

procedure erro(mensagem: string);
begin
    MessageDlg(mensagem, mtError, [mbOk], 0);
end;

function procuraColuna(grid: tdbgrid; campo: string): Integer;
var
    i, index: Integer;
begin
    index := -1;

    for i := 0 to grid.Columns.Count - 1 do begin
        if Uppercase(campo) = Uppercase(grid.Columns.Items[i].FieldName) then begin
            index := i;
        end;
    end;

    Result := index;
end;


end.
