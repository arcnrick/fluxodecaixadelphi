object fParametros: TfParametros
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Par'#226'metros'
  ClientHeight = 183
  ClientWidth = 389
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 22
    Top = 53
    Width = 90
    Height = 13
    Caption = 'Nome da Empresa:'
  end
  object parNomeEmpresa: TEdit
    Left = 22
    Top = 72
    Width = 347
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 30
    TabOrder = 0
  end
  object parUsaCCustoRec: TCheckBox
    Left = 22
    Top = 112
    Width = 185
    Height = 17
    Caption = 'Usa Centro de Custo na Receita'
    TabOrder = 1
  end
  object parUsaCCustopag: TCheckBox
    Left = 22
    Top = 144
    Width = 185
    Height = 17
    Caption = 'Usa Centro de Custo na Despesa'
    TabOrder = 2
  end
  object btGravar: TButton
    Left = 22
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Gravar'
    TabOrder = 3
    OnClick = btGravarClick
  end
  object qryParametros: TADOQuery
    Connection = fDm.Conexao
    Parameters = <>
    Left = 336
    Top = 8
  end
end
