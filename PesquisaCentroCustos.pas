unit PesquisaCentroCustos;

interface

uses
    Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
    System.Classes, Vcl.Graphics,
    Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, Vcl.Grids,
    Vcl.DBGrids;

type
    TfPesquisaCentroCustos = class(TForm)
        qryCentroCusto: TADOQuery;
        DBGrid1: TDBGrid;
        dsCentroCusto: TDataSource;
        procedure DBGrid1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    private
        { Private declarations }
    public
        { Public declarations }
        mostraInativo: Boolean;
        titTipo: Integer;
    end;

var
    fPesquisaCentroCustos: TfPesquisaCentroCustos;

implementation

{$R *.dfm}

uses Dm, Funcoes;

procedure TfPesquisaCentroCustos.DBGrid1DblClick(Sender: TObject);
begin
    if qryCentroCusto.RecNo > 0 then
    begin
        tag := 1;
        Close;
    end;
end;

procedure TfPesquisaCentroCustos.FormCreate(Sender: TObject);
begin
    mostraInativo := True;
    titTipo := -1;
end;

procedure TfPesquisaCentroCustos.FormShow(Sender: TObject);
begin
    qryCentroCusto.Close;
    qryCentroCusto.SQL.Text :=
        ' select ' +
        ' id, ccuDescricao, ' +
        ' case when ccuReceita = ''S'' then ' +
        '     ''Sim'' ' +
        ' else ' +
        '     ''N�o'' end ccuReceita, ' +
        ' case when ccuDespesa = ''S'' then ' +
        '     ''Sim'' ' +
        ' else ' +
        '     ''N�o'' end ccuDespesa, ' +
        ' case when ccuInativo = ''S'' then ' +
        '    ''Sim'' ' +
        ' else ' +
        '     ''N�o'' end ccuInativo ' +
        ' from tbCentroCustos ' +
        ' where 1 = 1 ';
    qryCentroCusto.Open;

    if not mostraInativo then begin
        qryCentroCusto.SQL.Text := qryCentroCusto.SQL.Text +
            ' and isnull(ccuInativo, '''') = ''N'' ';
    end;

    if titTipo = 0 then begin
        qryCentroCusto.SQL.Text := qryCentroCusto.SQL.Text +
            ' and isnull(ccuDespesa, '''') = ''S'' ';
    end else if titTipo = 1 then begin
        qryCentroCusto.SQL.Text := qryCentroCusto.SQL.Text +
            ' and isnull(ccuReceita, '''') = ''S'' ';
    end;

    qryCentroCusto.SQL.Text := qryCentroCusto.SQL.Text +
        ' order by ccuDescricao ';
    qryCentroCusto.Open;

    if not mostraInativo then begin
        DBGrid1.Columns[procuraColuna(DBGrid1, 'ccuInativo')].Visible := False;
    end;
end;

end.
