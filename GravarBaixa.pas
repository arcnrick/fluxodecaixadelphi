unit GravarBaixa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.Mask;

type
  TfGravarBaixa = class(TForm)
    lbValorPago: TLabel;
    titValorRecPag: TEdit;
    lbPagoDia: TLabel;
    titDataRecPag: TMaskEdit;
    Label8: TLabel;
    titDataVencimento: TMaskEdit;
    Label6: TLabel;
    titValor: TEdit;
    btCancelar: TBitBtn;
    btGravar: TBitBtn;
    procedure titValorExit(Sender: TObject);
    procedure titValorRecPagExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btGravarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fGravarBaixa: TfGravarBaixa;

implementation

{$R *.dfm}

procedure TfGravarBaixa.btCancelarClick(Sender: TObject);
begin
    Close;
end;

procedure TfGravarBaixa.btGravarClick(Sender: TObject);
begin
    Tag := 1;
    Close;
end;

procedure TfGravarBaixa.FormCreate(Sender: TObject);
begin
    Tag := 0;
end;

procedure TfGravarBaixa.titValorExit(Sender: TObject);
begin
    titValor.Text := FormatFloat('##0.00', StrToFloatDef(titValor.Text, 0));
end;

procedure TfGravarBaixa.titValorRecPagExit(Sender: TObject);
begin
    titValorRecPag.Text := FormatFloat('##0.00', StrToFloatDef(titValorRecPag.Text, 0));
end;

end.
