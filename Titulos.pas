unit Titulos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls,
  Data.DB, Data.Win.ADODB, Vcl.Mask, StrUtils, Funcoes, Vcl.ComCtrls, Math;

type
  TfTitulos = class(TForm)
    Panel1: TPanel;
    btGravar: TBitBtn;
    btPesquisar: TBitBtn;
    btCancelar: TBitBtn;
    btExcluir: TBitBtn;
    btLimpar: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    titPessoa: TEdit;
    id: TEdit;
    titTipo: TRadioGroup;
    qryTitulos: TADOQuery;
    Label5: TLabel;
    titReferencia: TEdit;
    Label6: TLabel;
    titValor: TEdit;
    Label7: TLabel;
    Label8: TLabel;
    lbPagoDia: TLabel;
    lbValorPago: TLabel;
    titValorRecPag: TEdit;
    titObservacao: TMemo;
    titDataCriacao: TMaskEdit;
    titDataVencimento: TMaskEdit;
    titDataRecPag: TMaskEdit;
    titPessoaDesc: TEdit;
    sbPessoa: TSpeedButton;
    qryBusca: TADOQuery;
    Label3: TLabel;
    sbCentro: TSpeedButton;
    titCentroCusto: TEdit;
    titCentroCustoDesc: TEdit;
    procedure FormShow(Sender: TObject);
    procedure btGravarClick(Sender: TObject);
    procedure btLimparClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure titTipoClick(Sender: TObject);
    procedure sbPessoaClick(Sender: TObject);
    procedure sbCentroClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure titValorExit(Sender: TObject);
    procedure titValorRecPagExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
        baixado, usaCCPag, usaCCRec: Boolean;
        procedure carregaDados(cod: integer);
        procedure limpaTela;
  public
    { Public declarations }
    codigoDaBaixa: Integer;
  end;

var
  fTitulos: TfTitulos;

implementation

{$R *.dfm}

uses PesquisaPessoas, PesquisaCentroCustos, PesquisaTitulos;

{ TfTitulos }

procedure TfTitulos.btCancelarClick(Sender: TObject);
begin
    if Trim(id.Text) = '0' then begin
        limpaTela;
    end else begin
        carregaDados(StrToInt(id.Text));
    end;
end;

procedure TfTitulos.btExcluirClick(Sender: TObject);
begin
    if Trim(id.Text) = '0' then begin
        alerta('N�o � poss�vel excluir o c�digo 0 (zero)');
        abort;
    end;

    if Confirma('Deseja realmente excluir o registro?') then begin
        qryTitulos.Close;
        qryTitulos.SQL.Text :=
            ' delete from tbPessoas ' +
            ' where id = :id ';
        qryTitulos.Parameters.ParamByName('id').Value := Trim(id.Text);
        qryTitulos.ExecSQL;

        mensagem('Exclu�do com sucesso');
        limpaTela;
    end;
end;

procedure TfTitulos.btGravarClick(Sender: TObject);
var
    insercao, pagoRecebido: Boolean;
    recPag: string;
begin
    insercao := False;
    pagoRecebido := False;
    recPag := IfThen(titTipo.ItemIndex = 0, 'P', 'R');

    if Trim(titDataCriacao.Text) = '/  /' then begin
        alerta('Informe a data do t�tulo.');
        titDataCriacao.SetFocus;
        Abort;
    end;

    if Trim(titPessoa.Text) = '' then begin
        alerta('Informe a pessoa.');
        titPessoa.SetFocus;
        Abort;
    end;

    if ((titTipo.ItemIndex = 0) and (usaCCPag)) or
    ((titTipo.ItemIndex = 1) and (usaCCRec)) then begin
        if Trim(titCentroCusto.Text) = '' then begin
            alerta('Informe o centro de custo.');
            titCentroCusto.SetFocus;
            Abort;
        end;
    end;

    if StrToFloatDef(titValor.Text, 0) <= 0 then begin
        alerta('Informe o valor do t�tulo.');
        titValor.SetFocus;
        Abort;
    end;

    if Trim(titDataVencimento.Text) = '/  /' then begin
        alerta('Informe a data de vencimento do t�tulo.');
        titDataVencimento.SetFocus;
        Abort;
    end;

    if (StrToFloatDef(titValorRecPag.Text, 0) > 0) or (Trim(titDataRecPag.Text) <> '/  /') then begin
        if StrToFloatDef(titValorRecPag.Text, 0) <= 0 then begin
            if recPag = 'P' then
                alerta('Informe o valor pago do t�tulo.')
            else
                alerta('Informe o valor recebido do t�tulo.');
            titValorRecPag.SetFocus;
            Abort;
        end;

        if Trim(titDataRecPag.Text) = '/  /' then begin
            if recPag = 'P' then
                alerta('Informe a data de pagamento do t�tulo.')
            else
                alerta('Informe a data de recebimento do t�tulo.');
            titDataRecPag.SetFocus;
            Abort;
        end;
    end;

    if (baixado) and (StrToFloatDef(titValorRecPag.Text, 0) = 0) then begin
        if not confirma('O t�tulo estava baixado.' + #13 +
        'Se continuar a grava��o, a baixa ser� estornada.' + #13#13 +
        'Deseja continuar???') then begin
            Abort;
        end;
    end;

    pagoRecebido := (StrToFloatDef(titValorRecPag.Text, 0) > 0);

    insercao := Trim(id.Text) = '0';

    qryTitulos.Close;

    if insercao then begin
        qryTitulos.SQL.Text :=
            ' insert tbtitulos (' +
            ' titTipo, titDataCriacao, titPessoa, titCentroCusto, ' +
            ' titReferencia, titValor, titDataVencimento,  ' +
            ' titValorRecPag, titDataRecPag, titObservacao ' +
            ' ) values ( ' +
            ' :titTipo, :titDataCriacao, :titPessoa, :titCentroCusto, ' +
            ' :titReferencia, :titValor, :titDataVencimento,  ' +
            ' :titValorRecPag, :titDataRecPag, :titObservacao ) ';
    end else begin
        qryTitulos.SQL.Text :=
            ' update tbtitulos ' +
            ' set titTipo = :titTipo ' +
            ' , titDataCriacao = :titDataCriacao ' +
            ' , titPessoa = :titPessoa ' +
            ' , titCentroCusto = :titCentroCusto ' +
            ' , titReferencia = :titReferencia ' +
            ' , titValor = :titValor ' +
            ' , titDataVencimento = :titDataVencimento ' +
            ' , titValorRecPag = :titValorRecPag ' +
            ' , titDataRecPag = :titDataRecPag ' +
            ' , titObservacao = :titObservacao ' +
            ' where id = :id ';
        qryTitulos.Parameters.ParamByName('id').Value := Trim(id.Text);
    end;

    qryTitulos.Parameters.ParamByName('titTipo').Value := IfThen(titTipo.ItemIndex = 0, 'P', 'R');
    qryTitulos.Parameters.ParamByName('titDataCriacao').Value := Trim(titDataCriacao.Text);
    qryTitulos.Parameters.ParamByName('titPessoa').Value := Trim(titPessoa.Text);
    if Trim(titCentroCusto.Text) <> '' then
        qryTitulos.Parameters.ParamByName('titCentroCusto').Value := Trim(titCentroCusto.Text)
    else
        qryTitulos.Parameters.ParamByName('titCentroCusto').Value := null;
    qryTitulos.Parameters.ParamByName('titReferencia').Value := Trim(titReferencia.Text);
    qryTitulos.Parameters.ParamByName('titValor').Value := StrToFloatDef(titValor.Text, 0);
    qryTitulos.Parameters.ParamByName('titDataVencimento').Value := Trim(titDataVencimento.Text);

    if pagoRecebido then begin
        qryTitulos.Parameters.ParamByName('titValorRecPag').Value := StrToFloatDef(titValorRecPag.Text, 0);
        qryTitulos.Parameters.ParamByName('titDataRecPag').Value := Trim(titDataRecPag.Text);
    end else begin
        qryTitulos.Parameters.ParamByName('titValorRecPag').Value := 0;
        qryTitulos.Parameters.ParamByName('titDataRecPag').Value := null;
    end;

    qryTitulos.Parameters.ParamByName('titObservacao').Value := Trim(titObservacao.Text);
    qryTitulos.ExecSQL;

    mensagem('Salvo com sucesso.');
    limpaTela;
end;

procedure TfTitulos.btLimparClick(Sender: TObject);
begin
    limpaTela;
end;

procedure TfTitulos.btPesquisarClick(Sender: TObject);
var
    codigo: Integer;
begin
    codigo := 0;

    try
        fPesquisaTitulos := TfPesquisaTitulos.Create(Self);
        fPesquisaTitulos.ShowModal;

        if fPesquisaTitulos.Tag = 1 then begin
            codigo := fPesquisaTitulos.qryTitulos.FieldByName('id').AsInteger;
        end;
    finally
        FreeAndNil(fPesquisaTitulos);
    end;

    if codigo > 0 then begin
        carregaDados(codigo);
    end;
end;

procedure TfTitulos.carregaDados(cod: integer);
begin
    qryTitulos.Close;
    qryTitulos.SQL.Text :=
        ' select tit.id, titTipo, titDataCriacao, titPessoa, pesFantasia, ' +
        ' titCentroCusto, ccuDescricao, titReferencia, titValor, titDataVencimento, ' +
        ' titValorRecPag, titDataRecPag, titObservacao ' +
        ' from tbTitulos tit ' +
        ' left outer join tbPessoas pes on pes.id = tit.titPessoa ' +
        ' left outer join tbCentroCustos ccu on ccu.id = tit.titCentroCusto ' +
        ' where tit.id = :id ';
    qryTitulos.Parameters.ParamByName('id').Value := cod;
    qryTitulos.Open;

    if qryTitulos.RecordCount > 0 then begin
        limpaTela;

        id.Text := Trim(qryTitulos.FieldByName('id').AsString);
        titTipo.ItemIndex := IfThen(Trim(qryTitulos.FieldByName('titTipo').AsString) = 'P', 0, 1);
        titDataCriacao.Text := Trim(qryTitulos.FieldByName('titDataCriacao').AsString);
        titPessoa.Text := Trim(qryTitulos.FieldByName('titPessoa').AsString);
        titPessoaDesc.Text := Trim(qryTitulos.FieldByName('pesFantasia').AsString);
        titCentroCusto.Text := Trim(qryTitulos.FieldByName('titCentroCusto').AsString);
        titCentroCustoDesc.Text := Trim(qryTitulos.FieldByName('ccuDescricao').AsString);
        titReferencia.Text := Trim(qryTitulos.FieldByName('titReferencia').AsString);
        titValor.Text := FormatFloat('##.00', qryTitulos.FieldByName('titValor').AsFloat);
        titValorExit(Self);
        titDataVencimento.Text := qryTitulos.FieldByName('titDataVencimento').AsString;
        titValorRecPag.Text := FormatFloat('##.00', qryTitulos.FieldByName('titValorRecPag').AsFloat);
        titValorRecPagExit(Self);
        titDataRecPag.Text := qryTitulos.FieldByName('titDataRecPag').AsString;
        titObservacao.Text := Trim(qryTitulos.FieldByName('titObservacao').AsString);

        baixado := qryTitulos.FieldByName('titValorRecPag').AsFloat > 0;
    end;

    qryTitulos.Close;
end;

procedure TfTitulos.FormCreate(Sender: TObject);
begin
    baixado := False;
    codigoDaBaixa := 0;

    qryBusca.Close;
    qryBusca.Sql.Text :=
        ' select ' +
        ' parUsaCCustoPag, parUsaCCustoRec ' +
        ' from tbParametros ';
    qryBusca.open;

    usaCCPag := Trim(qryBusca.FieldByName('parUsaCCustoPag').AsString) = 'S';
    usaCCRec := Trim(qryBusca.FieldByName('parUsaCCustoRec').AsString) = 'S';
end;

procedure TfTitulos.FormShow(Sender: TObject);
begin
    limpaTela;

    if codigoDaBaixa > 0 then
        carregaDados(codigoDaBaixa);
end;

procedure TfTitulos.limpaTela;
begin
    id.Text := '0';
    titTipo.ItemIndex := 0;
    titTipoClick(Self);
    titDataCriacao.Text := FormatDateTime('dd/mm/yyyy', Date);
    titPessoa.Clear;
    titPessoaDesc.Clear;
    titCentroCusto.Clear;
    titCentroCustoDesc.Clear;
    titReferencia.Clear;
    titValor.Text := '0,00';
    titDataVencimento.Clear;
    titValorRecPag.Text := '0,00';
    titDataRecPag.Clear;
    titObservacao.Lines.Clear;
    titDataCriacao.SetFocus;
    baixado := False;
end;

procedure TfTitulos.sbPessoaClick(Sender: TObject);
begin
    try
        fPesquisaPessoas := TfPesquisaPessoas.Create(Self);
        fPesquisaPessoas.mostraInativo := False;
        fPesquisaPessoas.ShowModal;

        if fPesquisaPessoas.Tag = 1 then begin
            titPessoa.Text := Trim(fPesquisaPessoas.qryPessoa.FieldByName('id').AsString);
            titPessoaDesc.Text := Trim(fPesquisaPessoas.qryPessoa.FieldByName('pesfantasia').AsString);
        end;
    finally
        FreeAndNil(fPesquisaPessoas);
    end;
end;

procedure TfTitulos.sbCentroClick(Sender: TObject);
begin
    try
        fPesquisaCentroCustos := TfPesquisaCentroCustos.Create(Self);
        fPesquisaCentroCustos.mostraInativo := False;
        fPesquisaCentroCustos.titTipo := titTipo.ItemIndex;
        fPesquisaCentroCustos.ShowModal;

        if fPesquisaCentroCustos.Tag = 1 then begin
            titCentroCusto.Text := Trim(fPesquisaCentroCustos.qryCentroCusto.FieldByName('id').AsString);
            titCentroCustoDesc.Text := Trim(fPesquisaCentroCustos.qryCentroCusto.FieldByName('ccuDescricao').AsString);
        end;
    finally
        FreeAndNil(fPesquisaPessoas);
    end;
end;

procedure TfTitulos.titTipoClick(Sender: TObject);
begin
    if titTipo.ItemIndex = 0 then begin
        lbValorPago.Caption := 'Valor R$ Pago:';
        lbPagoDia.Caption := 'Pago dia:';

        if usaCCPag then begin
            sbCentro.Visible := True;
            titCentroCustoDesc.Color := clWindow;
        end else begin
            sbCentro.Visible := False;
            titCentroCustoDesc.Color := $00EEEEEE;
        end;
    end else begin
        lbValorPago.Caption := 'Valor R$ Receb:';
        lbPagoDia.Caption := 'Recebido dia:';

        sbCentro.Visible := usaCCRec;
        titCentroCustoDesc.Color := IfThen(usaCCRec, clWindow, $00EEEEEE);
    end;
end;

procedure TfTitulos.titValorExit(Sender: TObject);
begin
    titValor.Text := FormatFloat('##0.00', StrToFloatDef(titValor.Text, 0));
end;

procedure TfTitulos.titValorRecPagExit(Sender: TObject);
begin
    titValorRecPag.Text := FormatFloat('##0.00', StrToFloatDef(titValorRecPag.Text, 0));
end;

end.
