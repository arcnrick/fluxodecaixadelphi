object fGravarBaixa: TfGravarBaixa
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Gravar Baixa'
  ClientHeight = 109
  ClientWidth = 442
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lbValorPago: TLabel
    Left = 231
    Top = 51
    Width = 71
    Height = 13
    Caption = 'Valor R$ Pago:'
  end
  object lbPagoDia: TLabel
    Left = 329
    Top = 51
    Width = 45
    Height = 13
    Caption = 'Pago dia:'
  end
  object Label8: TLabel
    Left = 118
    Top = 51
    Width = 59
    Height = 13
    Caption = 'Vencimento:'
  end
  object Label6: TLabel
    Left = 22
    Top = 51
    Width = 44
    Height = 13
    Caption = 'Valor R$:'
  end
  object titValorRecPag: TEdit
    Left = 231
    Top = 67
    Width = 73
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 60
    TabOrder = 2
    OnExit = titValorRecPagExit
  end
  object titDataRecPag: TMaskEdit
    Left = 329
    Top = 67
    Width = 86
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object titDataVencimento: TMaskEdit
    Left = 118
    Top = 67
    Width = 91
    Height = 21
    EditMask = '!99/99/0000;1;_'
    MaxLength = 10
    TabOrder = 1
    Text = '  /  /    '
  end
  object titValor: TEdit
    Left = 22
    Top = 67
    Width = 75
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 60
    TabOrder = 0
    OnExit = titValorExit
  end
  object btCancelar: TBitBtn
    Left = 110
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
      33333337777FF377FF3333993370739993333377FF373F377FF3399993000339
      993337777F777F3377F3393999707333993337F77737333337FF993399933333
      399377F3777FF333377F993339903333399377F33737FF33377F993333707333
      399377F333377FF3377F993333101933399377F333777FFF377F993333000993
      399377FF3377737FF7733993330009993933373FF3777377F7F3399933000399
      99333773FF777F777733339993707339933333773FF7FFF77333333999999999
      3333333777333777333333333999993333333333377777333333}
    NumGlyphs = 2
    TabOrder = 4
    TabStop = False
    OnClick = btCancelarClick
  end
  object btGravar: TBitBtn
    Left = 22
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Gravar'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333FFFFFFFFFFFFF33000077777770033377777777777773F000007888888
      00037F3337F3FF37F37F00000780088800037F3337F77F37F37F000007800888
      00037F3337F77FF7F37F00000788888800037F3337777777337F000000000000
      00037F3FFFFFFFFFFF7F00000000000000037F77777777777F7F000FFFFFFFFF
      00037F7F333333337F7F000FFFFFFFFF00037F7F333333337F7F000FFFFFFFFF
      00037F7F333333337F7F000FFFFFFFFF00037F7F333333337F7F000FFFFFFFFF
      00037F7F333333337F7F000FFFFFFFFF07037F7F33333333777F000FFFFFFFFF
      0003737FFFFFFFFF7F7330099999999900333777777777777733}
    NumGlyphs = 2
    TabOrder = 5
    TabStop = False
    OnClick = btGravarClick
  end
end
