unit Pessoas;

interface

uses
    Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
    System.Classes, Vcl.Graphics, StrUtils, Math,
    Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, Vcl.StdCtrls,
    Vcl.Buttons, Vcl.ExtCtrls, Vcl.Mask;

type
    TfPessoas = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    pesRazaoSocial: TEdit;
    pesInativa: TCheckBox;
    pesCliente: TCheckBox;
    pesFornecedor: TCheckBox;
    id: TEdit;
    qryPessoas: TADOQuery;
    Label3: TLabel;
    pesFantasia: TEdit;
    pesTipo: TRadioGroup;
    Label4: TLabel;
    Label5: TLabel;
    pesEmail: TEdit;
    Panel1: TPanel;
    btGravar: TBitBtn;
    btPesquisar: TBitBtn;
    btCancelar: TBitBtn;
    btExcluir: TBitBtn;
    btLimpar: TBitBtn;
    pesTelefone: TMaskEdit;
    procedure btGravarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btLimparClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    private
        { Private declarations }
        procedure carregaDados(cod: integer);
        procedure limpaTela;
    public
        { Public declarations }
    end;

var
    fPessoas: TfPessoas;

implementation

{$R *.dfm}

uses PesquisaPessoas, Funcoes;

procedure TfPessoas.btCancelarClick(Sender: TObject);
begin
    if Trim(id.Text) = '0' then begin
        limpaTela;
    end else begin
        carregaDados(StrToInt(id.Text));
    end;
end;

procedure TfPessoas.btExcluirClick(Sender: TObject);
begin
    if Trim(id.Text) = '0' then begin
        alerta('N�o � poss�vel excluir o c�digo 0 (zero)');
        abort;
    end;

    if Confirma('Deseja realmente excluir o registro?') then begin
        qryPessoas.Close;
        qryPessoas.SQL.Text :=
            ' delete from tbPessoas ' +
            ' where id = :id ';
        qryPessoas.Parameters.ParamByName('id').Value := Trim(id.Text);
        qryPessoas.ExecSQL;

        mensagem('Exclu�do com sucesso');
        limpaTela;
    end;

    //if MessageDlg('Deseja realmente excluir o registro?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
    //end;
end;

procedure TfPessoas.limpaTela;
begin
    id.Text := '0';
    pesRazaoSocial.Text := '';
    pesFantasia.Text := '';
    pesTipo.ItemIndex := 0;
    pesCliente.Checked := False;
    pesFornecedor.Checked := False;
    pesTelefone.Text := '';
    pesEmail.Text := '';
    pesInativa.Checked := False;

    pesRazaoSocial.SetFocus;
end;

procedure TfPessoas.btGravarClick(Sender: TObject);
var
    insercao: Boolean;
begin
    insercao := False;

    if Trim(pesRazaoSocial.Text) = '' then begin
        alerta('Informe a raz�o social da pessoa.');
        pesRazaoSocial.SetFocus;
        Abort;
    end;

    if Trim(pesFantasia.Text) = '' then begin
        alerta('Informe o nome fantasia da pessoa.');
        pesFantasia.SetFocus;
        Abort;
    end;

    if (not pesCliente.Checked) and (not pesFornecedor.Checked) then begin
        alerta('Selecione ao menos 1 (uma) das op��es: Clente / Fornecedor.');
        pesCliente.SetFocus;
        Abort;
    end;

    if Trim(pesTelefone.Text) = '(  )    -' then begin
        pesTelefone.Clear;  // tamb�m pode ser: pesTelefone.Text := '';
    end;

    if not (emailValido(Trim(pesEmail.Text))) then begin
        alerta('Email informado inv�lido');
        pesEmail.SetFocus;
        Abort;
    end;

    insercao := Trim(id.Text) = '0';

    qryPessoas.Close;

    if insercao then begin
        qryPessoas.SQL.Text :=
            ' insert tbPessoas (' +
            ' pesRazaoSocial, pesFantasia, pesTipo, pesCliente, ' +
            ' pesFornecedor, pesTelefone, pesEmail, pesInativa ' +
            ' ) values ( ' +
            ' :pesRazaoSocial, :pesFantasia, :pesTipo, :pesCliente, ' +
            ' :pesFornecedor, :pesTelefone, :pesEmail, :pesInativa ) ';
    end else begin
        qryPessoas.SQL.Text :=
            ' update tbPessoas ' +
            ' set pesRazaoSocial = :pesRazaoSocial ' +
            ' , pesFantasia = :pesFantasia ' +
            ' , pesTipo = :pesTipo ' +
            ' , pesCliente = :pesCliente ' +
            ' , pesFornecedor = :pesFornecedor ' +
            ' , pesTelefone = :pesTelefone ' +
            ' , pesEmail = :pesEmail ' +
            ' , pesInativa = :pesInativa ' +
            ' where id = :id ';
        qryPessoas.Parameters.ParamByName('id').Value := Trim(id.Text);
    end;

    qryPessoas.Parameters.ParamByName('pesRazaoSocial').Value := Trim(pesRazaoSocial.Text);
    qryPessoas.Parameters.ParamByName('pesFantasia').Value := Trim(pesFantasia.Text);
    qryPessoas.Parameters.ParamByName('pesTipo').Value := IfThen(pesTipo.ItemIndex = 0, 'F', 'J');  //StrUtils,
    qryPessoas.Parameters.ParamByName('pesCliente').Value := IfThen(pesCliente.Checked, 'S', 'N');
    qryPessoas.Parameters.ParamByName('pesFornecedor').Value := IfThen(pesFornecedor.Checked, 'S', 'N');
    qryPessoas.Parameters.ParamByName('pesTelefone').Value := Trim(pesTelefone.Text);
    qryPessoas.Parameters.ParamByName('pesEmail').Value := Trim(pesEmail.Text);
    qryPessoas.Parameters.ParamByName('pesInativa').Value := IfThen(pesInativa.Checked, 'S', 'N');
    qryPessoas.ExecSQL;

    mensagem('Salvo com sucesso.');
    limpaTela;
end;

procedure TfPessoas.btLimparClick(Sender: TObject);
begin
    limpaTela;
end;

procedure TfPessoas.btPesquisarClick(Sender: TObject);
var
    codigo: Integer;
begin
    codigo := 0;

    try
        fPesquisaPessoas := TfPesquisaPessoas.Create(Self);
        fPesquisaPessoas.ShowModal;

        if fPesquisaPessoas.Tag = 1 then begin
            codigo := fPesquisaPessoas.qryPessoa.FieldByName('id').AsInteger;
        end;
    finally
        FreeAndNil(fPesquisaPessoas);
    end;

    if codigo > 0 then begin
        carregaDados(codigo);
    end;
end;

procedure TfPessoas.carregaDados(cod: integer);
begin
    qryPessoas.Close;
    qryPessoas.SQL.Text :=
        ' select id, pesRazaoSocial, pesFantasia, pesTipo, ' +
        ' pesCliente, pesFornecedor, pesTelefone, pesEmail, pesInativa ' +
        ' from tbPessoas ' +
        ' where id = :id ';
    qryPessoas.Parameters.ParamByName('id').Value := cod;
    qryPessoas.Open;

    if qryPessoas.RecordCount > 0 then begin

        limpaTela;

        id.Text := Trim(qryPessoas.FieldByName('id').AsString);
        pesRazaoSocial.Text := Trim(qryPessoas.FieldByName('pesRazaoSocial').AsString);
        pesFantasia.Text := Trim(qryPessoas.FieldByName('pesFantasia').AsString);

        if Trim(qryPessoas.FieldByName('pesTipo').AsString) = 'F' then
            pesTipo.ItemIndex := 0
        else
            pesTipo.ItemIndex := 1;

        //pesTipo.ItemIndex := IfThen(Trim(qryPessoas.FieldByName('pesTipo').AsString) = 'F', 0, 1);

        pesCliente.Checked := Trim(qryPessoas.FieldByName('pesCliente').AsString) = 'S';
        pesFornecedor.Checked := Trim(qryPessoas.FieldByName('pesFornecedor').AsString) = 'S';
        pesTelefone.Text := Trim(qryPessoas.FieldByName('pesTelefone').AsString);
        pesEmail.Text := LowerCase(Trim(qryPessoas.FieldByName('pesEmail').AsString));
        pesInativa.Checked := Trim(qryPessoas.FieldByName('pesInativa').AsString) = 'S';
    end;

    qryPessoas.Close;
end;

procedure TfPessoas.FormShow(Sender: TObject);
begin
    limpaTela;
end;

end.
