unit Menu;

interface

uses
    Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
    System.Classes, Vcl.Graphics, Funcoes,
    Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Menus;

type
    TfMenu = class(TForm)
    MainMenu1: TMainMenu;
    Cadastros1: TMenuItem;
    Movimentaes1: TMenuItem;
    Relatrios1: TMenuItem;
    Sair1: TMenuItem;
    Parmetros1: TMenuItem;
    Pessoas1: TMenuItem;
    CentrodeCustos1: TMenuItem;
    tulos1: TMenuItem;
    BaixadeTtulos1: TMenuItem;
    tulosaReceber1: TMenuItem;
    tulosaPagar1: TMenuItem;
    FluxodeCaixa1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure Sair1Click(Sender: TObject);
    procedure Parmetros1Click(Sender: TObject);
    procedure Pessoas1Click(Sender: TObject);
    procedure CentrodeCustos1Click(Sender: TObject);
    procedure tulos1Click(Sender: TObject);
    procedure BaixadeTtulos1Click(Sender: TObject);
    procedure tulosaPagar1Click(Sender: TObject);
    procedure tulosaReceber1Click(Sender: TObject);
    procedure FluxodeCaixa1Click(Sender: TObject);
    private
        { Private declarations }
    public
        { Public declarations }
    end;

var
    fMenu: TfMenu;

implementation

{$R *.dfm}

uses Parametros, Pessoas, CentroCustos, Titulos, Dm, BaixaTitulos, Relatorios,
  FluxoCaixa;

procedure TfMenu.BaixadeTtulos1Click(Sender: TObject);
begin
    try
        fBaixaTitulos := TfBaixaTitulos.Create(Self);
        fBaixaTitulos.ShowModal;
    finally
        FreeAndNil(fBaixaTitulos);
    end;
end;

procedure TfMenu.CentrodeCustos1Click(Sender: TObject);
begin
    try
        fCentroCustos := TfCentroCustos.Create(Self);
        fCentroCustos.ShowModal;
    finally
        FreeAndNil(fCentroCustos);
    end;
end;

procedure TfMenu.FluxodeCaixa1Click(Sender: TObject);
begin
    try
        fFluxoCaixa := TfFluxoCaixa.Create(Self);
        fFluxoCaixa.ShowModal;
    finally
        FreeAndNil(fFluxoCaixa);
    end;
end;

procedure TfMenu.FormCreate(Sender: TObject);
begin
    if not fdm.conectado then begin
        showmessage('Falhou ao conectar.');
        application.Terminate;
    end;
end;

procedure TfMenu.Parmetros1Click(Sender: TObject);
begin
    try
        fParametros := TfParametros.Create(Self);
        fParametros.ShowModal;
    finally
        FreeAndNil(fParametros);
    end;
end;

procedure TfMenu.Pessoas1Click(Sender: TObject);
begin
    try
        fPessoas := TfPessoas.Create(Self);
        fPessoas.ShowModal;
    finally
        FreeAndNil(fPessoas);
    end;
end;

procedure TfMenu.Sair1Click(Sender: TObject);
begin
    if confirma('Deseja realmente sair?') then
        Application.Terminate;
end;

procedure TfMenu.tulos1Click(Sender: TObject);
begin
    try
        fTitulos := TfTitulos.Create(Self);
        fTitulos.ShowModal;
    finally
        FreeAndNil(fTitulos);
    end;
end;

procedure TfMenu.tulosaPagar1Click(Sender: TObject);
begin
    try
        fRelatorios := TfRelatorios.Create(Self);
        fRelatorios.pagOuRec := 'P';
        fRelatorios.ShowModal;
    finally
        FreeAndNil(fRelatorios);
    end;
end;

procedure TfMenu.tulosaReceber1Click(Sender: TObject);
begin
    try
        fRelatorios := TfRelatorios.Create(Self);
        fRelatorios.pagOuRec := 'R';
        fRelatorios.ShowModal;
    finally
        FreeAndNil(fRelatorios);
    end;
end;

end.
