program FluxoDeCaixa;

uses
  Vcl.Forms,
  Menu in 'Menu.pas' {fMenu},
  Parametros in 'Parametros.pas' {fParametros},
  Pessoas in 'Pessoas.pas' {fPessoas},
  CentroCustos in 'CentroCustos.pas' {fCentroCustos},
  Titulos in 'Titulos.pas' {fTitulos},
  Dm in 'Dm.pas' {fDm: TDataModule},
  PesquisaTitulos in 'PesquisaTitulos.pas' {fPesquisaTitulos},
  PesquisaPessoas in 'PesquisaPessoas.pas' {fPesquisaPessoas},
  Funcoes in 'Funcoes.pas',
  PesquisaCentroCustos in 'PesquisaCentroCustos.pas' {fPesquisaCentroCustos},
  BaixaTitulos in 'BaixaTitulos.pas' {fBaixaTitulos},
  GravarBaixa in 'GravarBaixa.pas' {fGravarBaixa},
  Relatorios in 'Relatorios.pas' {fRelatorios},
  FluxoCaixa in 'FluxoCaixa.pas' {fFluxoCaixa};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfDm, fDm);
  Application.CreateForm(TfMenu, fMenu);
  Application.Run;
end.
