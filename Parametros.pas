unit Parametros;

interface

uses
    Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
    System.Classes, Vcl.Graphics, StrUtils,
    Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB,
  Datasnap.DBClient, Vcl.DBCtrls, Vcl.StdCtrls, Vcl.Mask, Vcl.Grids, Vcl.DBGrids;

type
    TfParametros = class(TForm)
    qryParametros: TADOQuery;
    Label1: TLabel;
    parNomeEmpresa: TEdit;
    parUsaCCustoRec: TCheckBox;
    parUsaCCustopag: TCheckBox;
    btGravar: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btGravarClick(Sender: TObject);
    private
        { Private declarations }
    public
        { Public declarations }
    end;

var
    fParametros: TfParametros;

implementation

{$R *.dfm}

uses Dm;

procedure TfParametros.btGravarClick(Sender: TObject);
begin
    if Trim(parNomeEmpresa.Text) = '' then begin
        showmessage('Informe o nome da empresa.');
        parNomeEmpresa.SetFocus;
        Abort;
    end;

    if Length(Trim(parNomeEmpresa.Text)) > 30 then begin
        showmessage('Nome da empresa ultrapassou 30 caracteres.');
        parNomeEmpresa.SetFocus;
        Abort;
    end;

    qryParametros.Close;
    qryParametros.SQL.Text :=
        ' update tbParametros ' +
        ' set parNomeEmpresa = :parNomeEmpresa ' +
        ' , parUsaCCustoRec = :parUsaCCustoRec ' +
        ' , parUsaCCustoPag = :parUsaCCustoPag ';

    qryParametros.Parameters.ParamByName('parNomeEmpresa').Value := Trim(parNomeEmpresa.Text);

    if parUsaCCustoRec.Checked then
        qryParametros.Parameters.ParamByName('parUsaCCustoRec').Value := 'S'
    else
        qryParametros.Parameters.ParamByName('parUsaCCustoRec').Value := 'N';

    qryParametros.Parameters.ParamByName('parUsaCCustoPag').Value := IfThen(parUsaCCustoPag.Checked, 'S', 'N');

    qryParametros.ExecSQL;

    showmessage('Salvo com sucesso.');
end;

procedure TfParametros.FormCreate(Sender: TObject);
begin
    qryParametros.Close;
    qryParametros.SQL.Text := 'select * from tbParametros';
    qryParametros.Open;
end;

procedure TfParametros.FormShow(Sender: TObject);
begin
    if qryParametros.RecordCount > 0 then begin
        parNomeEmpresa.Text := Trim(qryParametros.FieldByName('parNomeEmpresa').AsString);

        if Trim(qryParametros.FieldByName('parUsaCCustoRec').AsString) = 'S' then
            parUsaCCustoRec.Checked := True
        else
            parUsaCCustoRec.Checked := False;

        parUsaCCustopag.Checked := Trim(qryParametros.FieldByName('parUsaCCustoPag').AsString) = 'S';
    end;

    qryParametros.Close;
end;

end.
