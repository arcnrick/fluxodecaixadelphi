insert into tbPessoas
	(pesRazaoSocial, pesFantasia, pesTipo, pesCliente, pesFornecedor, pesEmail, pesInativa) 
values 
	('COMP. PAULISTA FORCA E LUZ', 'CPFL', 'J', 'N', 'S', 'email@cpfl.com', 'N');

insert into tbPessoas
	(pesRazaoSocial, pesFantasia, pesTipo, pesCliente, pesFornecedor, pesEmail) /* observação */
values 
	('COMP. TELECOMONICACOES', 'COMTELE', 'J', 'N', 'S', 'email@comtele.com');

insert into tbPessoas
	(pesRazaoSocial, pesFantasia, pesTipo, pesCliente, pesFornecedor, pesInativa) 
values 
	('SERV. ABASTECIMENTO AGUA LTDA', 'AGUA', 'J', 'N', 'S', 'N');

insert into tbPessoas
	(pesRazaoSocial, pesFantasia, pesTipo, pesCliente, pesFornecedor) 
values 
	('JAIR JARDINEIRO', 'JAIRZINHO', 'F', 'N', 'S');

insert into tbPessoas
	(pesRazaoSocial, pesFantasia, pesTipo, pesCliente, pesFornecedor, pesInativa) 
values 
	('SILVANA SILVA SANTOS', 'SILVINHA', 'F', 'N', 'S', 'N');

insert into tbPessoas
	(pesRazaoSocial, pesFantasia, pesTipo, pesCliente, pesFornecedor, pesInativa) 
values 
	('SOFTWARE PLUS - ME', 'SP', 'J', 'S', 'S', 'N');

insert into tbPessoas
	(pesRazaoSocial, pesFantasia, pesTipo, pesCliente, pesFornecedor, pesInativa) 
values 
	('PEDRO LAZARTO', 'PEDRAO', 'F', 'S', 'N', 'N');

insert into tbPessoas
	(pesRazaoSocial, pesFantasia, pesTipo, pesCliente, pesFornecedor, pesInativa) 
values 
	('ACADEMIA MAIS SAUDE LTDA', 'MAIS SAUDE', 'J', 'S', 'S', 'N');

insert into tbPessoas
	(pesRazaoSocial, pesFantasia, pesTipo, pesCliente, pesFornecedor, pesInativa) 
values 
	('SUPERMERCADO SANTOS E SANTOS', 'SANTOS MERCADO', 'J', 'N', 'S', 'N');

insert into tbPessoas
	(pesRazaoSocial, pesFantasia, pesTipo, pesCliente, pesFornecedor, pesInativa) 
values 
	('ZALIO BORRACHARIA MEI', 'ZAZ', 'J', 'N', 'S', 'N');

insert into tbPessoas
	(pesRazaoSocial, pesFantasia, pesTipo, pesCliente, pesFornecedor, pesInativa) 
values 
	('ANTONIO CARLOS JUNQUIEM', 'ACJ', 'J', 'S', 'N', 'N');

insert into tbPessoas
	(pesRazaoSocial, pesFantasia, pesTipo, pesCliente, pesFornecedor, pesInativa) 
values 
	('BICICLETARIA GOMES', 'BOLA', 'J', 'S', 'S', 'N');

insert into tbPessoas
	(pesRazaoSocial, pesFantasia, pesTipo, pesCliente, pesFornecedor, pesInativa) 
values 
	('RONDA NOTURNA OLHOS DE LINCE', 'RONDA', 'J', 'N', 'S', 'N');

insert into tbPessoas
	(pesRazaoSocial, pesFantasia, pesTipo, pesCliente, pesFornecedor, pesInativa) 
values 
	('PEREZ CABELEREIRO', 'PENINHA', 'J', 'N', 'S', 'S');

insert into tbPessoas
	(pesRazaoSocial, pesFantasia, pesTipo, pesCliente, pesFornecedor, pesInativa) 
values 
	('RICK JUNIOR', 'RJ', 'F', 'S', 'S', 'N');

insert into tbPessoas
	(pesRazaoSocial, pesFantasia, pesTipo, pesCliente, pesFornecedor, pesInativa) 
values 
	('AQUAVIVA ESCOLA DE NATACAO', 'AQUAVIVA', 'J', 'N', 'S', 'N');

insert into tbPessoas
	(pesRazaoSocial, pesFantasia, pesTipo, pesCliente, pesFornecedor, pesInativa) 
values 
	('MARIA MELO MENEZES', 'MARIINHA', 'F', 'S', 'N', 'N');

insert into tbPessoas
	(pesRazaoSocial, pesFantasia, pesTipo, pesCliente, pesFornecedor, pesInativa) 
values 
	('LUIS LEME ', 'LUISINHO', 'F', 'S', 'S', 'N');

insert into tbPessoas
	(pesRazaoSocial, pesFantasia, pesTipo, pesCliente, pesFornecedor, pesEmail, pesInativa) 
values 
	('NETNET LTDA', 'INTERNET', 'J', 'N', 'S', 'atendimento@netnet.com.br', 'N');

insert into tbPessoas
	(pesRazaoSocial, pesFantasia, pesTipo, pesCliente, pesFornecedor, pesInativa) 
values 
	('POSTO DE ABASTECIMENTO SILVA LTDA', 'POSTO MAIS', 'J', 'N', 'S', 'N');

insert into tbPessoas
	(pesRazaoSocial, pesFantasia, pesTipo, pesCliente, pesFornecedor, pesInativa) 
values 
	('BANKTOP SA', 'BANKTOP', 'J', 'N', 'S', 'N');

