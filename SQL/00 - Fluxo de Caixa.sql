create table tbParametros (
	parNomeEmpresa varchar(30),
	parUsaCCustoRec char(1),
	parUsaCCustoPag char(1)
);

create table tbPessoas (
	id int identity(1, 1) not null,
	pesRazaoSocial varchar (60),
	pesFantasia varchar (60),
	pesTipo char(1),
	pesCliente char(1),
	pesFornecedor char(1),
	pesTelefone varchar(15),
	pesEmail varchar(60),
	pesInativa char(1)
);

alter table tbPessoas add constraint pk_Pessoas primary key (id);

create table tbCentroCustos (
	id int identity(1, 1) not null,
	ccuDescricao varchar(60),
	ccuReceita char(1),	
	ccuDespesa char(1),	
	ccuInativo char(1)
);

alter table tbCentroCustos add constraint pk_CentroCustos primary key (id);

create table tbTitulos (
	id int identity(1, 1) not null,
	titTipo char(1),
	titDataCriacao datetime,
	titPessoa int not null,
	titCentroCusto int,
	titReferencia varchar(20),
	titValor money,
	titDataVencimento datetime,
	titValorRecPag money,
	titDataRecPag datetime,
	titObservacao varchar(300)
);

alter table tbTitulos add constraint pk_Titulos primary key (id);

alter table tbTitulos with nocheck add constraint fk_Titulos_Pessoas foreign key (titPessoa)
	references tbPessoas (id) on delete no action;

alter table tbTitulos with nocheck add constraint fk_Titulos_CentroCustos foreign key (titCentroCusto)
	references tbCentroCustos (id) on delete no action;









