unit PesquisaPessoas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, Vcl.Grids,
  Vcl.DBGrids;

type
  TfPesquisaPessoas = class(TForm)
    qryPessoa: TADOQuery;
    DBGrid1: TDBGrid;
    dsPessoa: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    mostraInativo: Boolean;
  end;

var
  fPesquisaPessoas: TfPesquisaPessoas;

implementation

{$R *.dfm}

uses Dm, Funcoes;

procedure TfPesquisaPessoas.DBGrid1DblClick(Sender: TObject);
begin
    if qryPessoa.RecNo > 0 then begin
        tag := 1;
        Close;
    end;
end;

procedure TfPesquisaPessoas.FormCreate(Sender: TObject);
begin
    mostraInativo := True;
end;

procedure TfPesquisaPessoas.FormShow(Sender: TObject);
begin
    qryPessoa.Close;
    qryPessoa.SQL.Text :=
        ' select id, pesRazaoSocial, pesFantasia, ' +
        ' case when pesTipo = ''J'' then ' +
        ' 	''Jur'' ' +
        ' else ' +
        ' 	''F�s'' end pesTipo, ' +
        ' case when pesCliente = ''S'' then ' +
        ' 	''Sim'' ' +
        ' else ' +
        ' 	''N�o'' end pesCliente, ' +
        ' case when pesFornecedor = ''S'' then ' +
        ' 	''Sim'' ' +
        ' else ' +
        ' 	''N�o'' end pesFornecedor, ' +
        ' isnull(pesTelefone, '''') pesTelefone, isnull(pesEmail, '''') pesEmail, ' +
        ' case when pesInativa = ''S'' then ' +
        ' 	''Sim'' ' +
        ' else ' +
        ' 	''N�o'' end pesInativa ' +
        ' from tbPessoas ';

    if not mostraInativo then begin
        qryPessoa.SQL.Text := qryPessoa.SQL.Text +
            'where isnull(pesInativa, '''') = ''N'' ';
    end;

    qryPessoa.SQL.Text := qryPessoa.SQL.Text +
        ' order by pesFantasia ';
    qryPessoa.Open;

    if not mostraInativo then begin
        DBGrid1.Columns[procuraColuna(DBGrid1, 'pesInativa')].Visible := False;
    end;
end;

end.
