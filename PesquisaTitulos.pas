unit PesquisaTitulos;

interface

uses
    Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
    System.Classes, Vcl.Graphics,
    Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, Vcl.Grids,
    Vcl.DBGrids;

type
    TfPesquisaTitulos = class(TForm)
    qryTitulos: TADOQuery;
        DBGrid1: TDBGrid;
    dsTitulos: TDataSource;
        procedure FormCreate(Sender: TObject);
        procedure DBGrid1DblClick(Sender: TObject);
    private
        { Private declarations }
    public
        { Public declarations }
    end;

var
    fPesquisaTitulos: TfPesquisaTitulos;

implementation

{$R *.dfm}

uses Dm;

procedure TfPesquisaTitulos.DBGrid1DblClick(Sender: TObject);
begin
    if qryTitulos.RecNo > 0 then begin
        tag := 1;
        Close;
    end;
end;

procedure TfPesquisaTitulos.FormCreate(Sender: TObject);
begin
    qryTitulos.Close;
    qryTitulos.SQL.Text :=
        ' select ' +
        ' tit.id, ' +
        ' case when titTipo = ''P'' then ' +
        ' 	''Pag'' ' +
        ' else ' +
        ' 	''Rec'' end titTipo, ' +
        ' titDataCriacao, titPessoa, pesFantasia, ' +
        ' titCentroCusto, ccuDescricao, titReferencia, ' +
        ' titValor, ' +
        ' titDataVencimento, ' +
        ' titValorRecPag, ' +
        ' titDataRecPag, titObservacao ' +
        ' from tbTitulos tit ' +
        ' left outer join tbPessoas pes on pes.id = tit.titPessoa ' +
        ' left outer join tbCentroCustos ccu on ccu.id = tit.titCentroCusto ' +
        ' order by titDataCriacao desc ';
    qryTitulos.Open;

    TBCDField(qryTitulos.FieldByName('titValor')).Currency := True;
    TBCDField(qryTitulos.FieldByName('titValorRecPag')).Currency := True;
end;

end.
