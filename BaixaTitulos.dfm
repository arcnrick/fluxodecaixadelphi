object fBaixaTitulos: TfBaixaTitulos
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Baixa de T'#237'tulos'
  ClientHeight = 527
  ClientWidth = 900
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 360
    Top = 32
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label2: TLabel
    Left = 272
    Top = 10
    Width = 59
    Height = 13
    Caption = 'Vencimento:'
  end
  object SpeedButton1: TSpeedButton
    Left = 838
    Top = 71
    Width = 54
    Height = 22
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
      3333333777333777FF33339993707399933333773337F3777FF3399933000339
      9933377333777F3377F3399333707333993337733337333337FF993333333333
      399377F33333F333377F993333303333399377F33337FF333373993333707333
      333377F333777F333333993333101333333377F333777F3FFFFF993333000399
      999377FF33777F77777F3993330003399993373FF3777F37777F399933000333
      99933773FF777F3F777F339993707399999333773F373F77777F333999999999
      3393333777333777337333333999993333333333377777333333}
    NumGlyphs = 2
    OnClick = SpeedButton1Click
  end
  object Label3: TLabel
    Left = 8
    Top = 53
    Width = 38
    Height = 13
    Caption = 'Pessoa:'
  end
  object sbPessoa: TSpeedButton
    Left = 379
    Top = 71
    Width = 23
    Height = 23
    Flat = True
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      33033333333333333F7F3333333333333000333333333333F777333333333333
      000333333333333F777333333333333000333333333333F77733333333333300
      033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
      33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
      3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
      33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
      333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
      333333773FF77333333333370007333333333333777333333333}
    NumGlyphs = 2
    OnClick = sbPessoaClick
  end
  object Label4: TLabel
    Left = 422
    Top = 53
    Width = 83
    Height = 13
    Caption = 'Centro de Custo:'
  end
  object sbCentro: TSpeedButton
    Left = 793
    Top = 71
    Width = 23
    Height = 23
    Flat = True
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      33033333333333333F7F3333333333333000333333333333F777333333333333
      000333333333333F777333333333333000333333333333F77733333333333300
      033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
      33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
      3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
      33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
      333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
      333333773FF77333333333370007333333333333777333333333}
    NumGlyphs = 2
    OnClick = sbCentroClick
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 112
    Width = 884
    Height = 407
    DataSource = dsTitulos
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    PopupMenu = PopupMenu1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnCellClick = DBGrid1CellClick
    OnDrawColumnCell = DBGrid1DrawColumnCell
    OnMouseMove = DBGrid1MouseMove
    Columns = <
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'Baixar'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'id'
        Title.Alignment = taRightJustify
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 45
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titTipo'
        Title.Caption = 'Tipo'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titDataCriacao'
        Title.Caption = 'Data'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titPessoa'
        Title.Caption = 'Pessoa'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 45
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'pesFantasia'
        Title.Caption = 'Nome Fantasia'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titCentroCusto'
        Title.Caption = 'C.Custo'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 45
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ccuDescricao'
        Title.Caption = 'Descri'#231#227'o'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titValor'
        Title.Caption = 'Valor'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titDataVencimento'
        Title.Caption = 'Vencto'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titValorRecPag'
        Title.Caption = 'Vr Pag/Rec'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titDataRecPag'
        Title.Caption = 'Pag / Rec'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titObservacao'
        Title.Caption = 'Observa'#231#227'o'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titReferencia'
        Title.Caption = 'Refer'#234'ncia'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = [fsBold]
        Width = 150
        Visible = True
      end>
  end
  object rgTipo: TRadioGroup
    Left = 8
    Top = 8
    Width = 249
    Height = 41
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Todos'
      'Pagamento'
      'Recebimento')
    TabOrder = 1
    OnClick = rgTipoClick
  end
  object dtInicio: TDateTimePicker
    Left = 272
    Top = 28
    Width = 85
    Height = 21
    Date = 43818.000000000000000000
    Time = 0.965958495369704900
    TabOrder = 2
  end
  object dtFim: TDateTimePicker
    Left = 384
    Top = 28
    Width = 85
    Height = 21
    Date = 43818.000000000000000000
    Time = 0.965958495369704900
    TabOrder = 3
  end
  object titPessoa: TEdit
    Left = 8
    Top = 72
    Width = 58
    Height = 21
    TabStop = False
    CharCase = ecUpperCase
    Color = 15658734
    MaxLength = 60
    NumbersOnly = True
    ReadOnly = True
    TabOrder = 4
  end
  object titPessoaDesc: TEdit
    Left = 68
    Top = 72
    Width = 310
    Height = 21
    TabStop = False
    CharCase = ecUpperCase
    MaxLength = 60
    ReadOnly = True
    TabOrder = 5
  end
  object titCentroCusto: TEdit
    Left = 422
    Top = 72
    Width = 58
    Height = 21
    TabStop = False
    CharCase = ecUpperCase
    Color = 15658734
    MaxLength = 60
    NumbersOnly = True
    ReadOnly = True
    TabOrder = 6
  end
  object titCentroCustoDesc: TEdit
    Left = 482
    Top = 72
    Width = 310
    Height = 21
    TabStop = False
    CharCase = ecUpperCase
    MaxLength = 60
    ReadOnly = True
    TabOrder = 7
  end
  object cbSomenteEmAberto: TCheckBox
    Left = 482
    Top = 30
    Width = 111
    Height = 17
    Caption = 'Somente em Aberto'
    TabOrder = 8
  end
  object qryTitulos: TADOQuery
    Connection = fDm.Conexao
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select '
      #39#39' Baixar, '
      'tit.id, '
      'case when titTipo = '#39'P'#39' then '
      #9#39'Pag'#39
      'else '
      #9#39'Rec'#39' end titTipo, '
      'titDataCriacao, '
      'titPessoa, '
      'pesFantasia,'
      'titCentroCusto, '
      'ccuDescricao,'
      'titReferencia, '
      'format(titValor, '#39'C'#39', '#39'pt-br'#39') titValor, '
      'titDataVencimento, '
      'titValorRecPag, '
      'titDataRecPag, '
      'titObservacao'
      'from tbTitulos tit'
      'left outer join tbPessoas pes on pes.id = tit.titPessoa'
      
        'left outer join tbCentroCustos ccu on ccu.id = tit.titCentroCust' +
        'o')
    Left = 632
    Top = 360
  end
  object dsTitulos: TDataSource
    DataSet = qryTitulos
    Left = 632
    Top = 280
  end
  object PopupMenu1: TPopupMenu
    Left = 65288
    Top = 320
    object AbrirCadastro: TMenuItem
      Caption = 'Abrir Cadastro'
      OnClick = AbrirCadastroClick
    end
  end
  object qryAuxiliar: TADOQuery
    Connection = fDm.Conexao
    CursorType = ctStatic
    Parameters = <>
    Left = 736
    Top = 360
  end
end
