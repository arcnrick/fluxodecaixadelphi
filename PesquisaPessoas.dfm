object fPesquisaPessoas: TfPesquisaPessoas
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Pesquisa de Pessoas'
  ClientHeight = 316
  ClientWidth = 548
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    548
    316)
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 8
    Top = 8
    Width = 532
    Height = 298
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = dsPessoa
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ParentFont = False
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    OnDblClick = DBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'id'
        Title.Alignment = taRightJustify
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'pesFantasia'
        Title.Caption = 'Fantasia'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'pesRazaoSocial'
        Title.Caption = 'Raz'#227'o Social'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'pesTipo'
        Title.Caption = 'Tipo'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'pesCliente'
        Title.Caption = 'Cliente'
        Width = 45
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'pesFornecedor'
        Title.Caption = 'Fornec'
        Width = 45
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'pesTelefone'
        Title.Caption = 'Telefone'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'pesEmail'
        Title.Caption = 'Email'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'pesInativa'
        Title.Caption = 'Inativa'
        Width = 50
        Visible = True
      end>
  end
  object qryPessoa: TADOQuery
    Connection = fDm.Conexao
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select id, pesFantasia, pesRazaoSocial, '
      'case when pesTipo = '#39'J'#39' then'
      #9#39'Jur'#39
      'else'
      #9#39'F'#237's'#39' end pesTipo,'
      'case when pesCliente = '#39'S'#39' then'
      #9#39'Sim'#39
      'else'
      #9#39'N'#227'o'#39' end pesCliente,'
      'case when pesFornecedor = '#39'S'#39' then'
      #9#39'Sim'#39
      'else'
      #9#39'N'#227'o'#39' end pesFornecedor,'
      
        'isnull(pesTelefone, '#39#39') pesTelefone, isnull(pesEmail, '#39#39') pesEma' +
        'il,'
      'case when pesInativa = '#39'S'#39' then'
      #9#39'Sim'#39
      'else'
      #9#39'N'#227'o'#39' end pesInativa'
      'from tbPessoas'
      'order by pesFantasia')
    Left = 368
    Top = 144
  end
  object dsPessoa: TDataSource
    DataSet = qryPessoa
    Left = 368
    Top = 200
  end
end
