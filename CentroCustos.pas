unit CentroCustos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Data.DB,
  Data.Win.ADODB, StrUtils, Vcl.ExtCtrls;

type
  TfCentroCustos = class(TForm)
    qryCentroCusto: TADOQuery;
    Label1: TLabel;
    ccuDescricao: TEdit;
    ccuInativo: TCheckBox;
    ccuDespesa: TCheckBox;
    ccuReceita: TCheckBox;
    Label2: TLabel;
    id: TEdit;
    btLimpar: TButton;
    btExcluir: TButton;
    btCancelar: TButton;
    btPesquisar: TButton;
    btGravar_: TButton;
    procedure btGravar_Click(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btLimparClick(Sender: TObject);
  private
    { Private declarations }
    procedure carregaDados(cod: integer);
    procedure limpaTela;
  public
    { Public declarations }
  end;

var
  fCentroCustos: TfCentroCustos;

implementation

{$R *.dfm}

uses PesquisaCentroCustos;

procedure TfCentroCustos.btExcluirClick(Sender: TObject);
begin
    if Trim(id.Text) = '0' then begin
        ShowMessage('N�o � poss�vel excluir o c�digo 0 (zero)');
        abort;
    end;

    if MessageDlg('Deseja realmente excluir o registro?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
        qryCentroCusto.Close;
        qryCentroCusto.SQL.Text :=
            ' delete from tbCentroCustos ' +
            ' where id = :id ';
        qryCentroCusto.Parameters.ParamByName('id').Value := Trim(id.Text);
        qryCentroCusto.ExecSQL;

        ShowMessage('Exclu�do com sucesso');
        limpaTela;
    end;
end;

procedure TfCentroCustos.btGravar_Click(Sender: TObject);
var
    insercao: Boolean;
begin
    insercao := False;

    if Trim(ccuDescricao.Text) = '' then begin
        showmessage('Informe a descri��o do centro de custo.');
        ccuDescricao.SetFocus;
        Abort;
    end;

    if Trim(id.Text) = '0' then begin
        insercao := True;
    end;

    //insercao := Trim(id.Text) = 0;

    qryCentroCusto.Close;

    if insercao then begin
        qryCentroCusto.SQL.Text :=
            ' insert tbCentroCustos (' +
            ' ccuDescricao, ccuInativo, ccuReceita, ccuDespesa ' +
            ' ) values ( ' +
            ' :ccuDescricao, :ccuInativo, :ccuReceita, :ccuDespesa ) ';
    end else begin
        qryCentroCusto.SQL.Text :=
            ' update tbCentroCustos ' +
            ' set ccuDescricao = :ccuDescricao ' +
            ' , ccuInativo = :ccuInativo ' +
            ' , ccuReceita = :ccuReceita ' +
            ' , ccuDespesa = :ccuDespesa ' +
            ' where id = :id ';
    end;

    qryCentroCusto.Parameters.ParamByName('ccuDescricao').Value := Trim(ccuDescricao.Text);
    qryCentroCusto.Parameters.ParamByName('ccuInativo').Value := IfThen(ccuInativo.Checked, 'S', 'N');
    qryCentroCusto.Parameters.ParamByName('ccuReceita').Value := IfThen(ccuReceita.Checked, 'S', 'N');
    qryCentroCusto.Parameters.ParamByName('ccuDespesa').Value := IfThen(ccuDespesa.Checked, 'S', 'N');

    if not insercao then begin
        qryCentroCusto.Parameters.ParamByName('id').Value := Trim(id.Text);
    end;

    qryCentroCusto.ExecSQL;

    showmessage('Salvo com sucesso.');
    limpaTela;
end;

procedure TfCentroCustos.btLimparClick(Sender: TObject);
begin
    limpaTela;
end;

procedure TfCentroCustos.btCancelarClick(Sender: TObject);
begin
    if Trim(id.Text) = '0' then begin
        limpaTela;
    end else begin
        carregaDados(StrToInt(id.Text));
    end;
end;

procedure TfCentroCustos.btPesquisarClick(Sender: TObject);
var
    codigo: Integer;
begin
    codigo := 0;

    try
        fPesquisaCentroCustos := TfPesquisaCentroCustos.Create(Self);
        fPesquisaCentroCustos.ShowModal;

        if fPesquisaCentroCustos.Tag = 1 then begin
            codigo := fPesquisaCentroCustos.qryCentroCusto.FieldByName('id').AsInteger;
        end;
    finally
        FreeAndNil(fPesquisaCentroCustos);
    end;

    if codigo > 0 then begin
        carregaDados(codigo);
    end;
end;

procedure TfCentroCustos.carregaDados(cod: integer);
begin
    qryCentroCusto.Close;
    qryCentroCusto.SQL.Text :=
        ' select ' +
        ' id, ccuDescricao, ccuReceita, ccuDespesa, ccuInativo ' +
        ' from tbCentroCustos ' +
        ' where id = :id ';
    qryCentroCusto.Parameters.ParamByName('id').Value := cod;
    qryCentroCusto.Open;

    if qryCentroCusto.RecordCount > 0 then begin

        limpaTela;

        id.Text := Trim(qryCentroCusto.FieldByName('id').AsString);
        ccuDescricao.Text := Trim(qryCentroCusto.FieldByName('ccuDescricao').AsString);
        ccuInativo.Checked := Trim(qryCentroCusto.FieldByName('ccuInativo').AsString) = 'S';
        ccuReceita.Checked := Trim(qryCentroCusto.FieldByName('ccuReceita').AsString) = 'S';
        ccuDespesa.Checked := Trim(qryCentroCusto.FieldByName('ccuDespesa').AsString) = 'S';
    end;

    qryCentroCusto.Close;
end;

procedure TfCentroCustos.FormShow(Sender: TObject);
begin
    limpaTela;
end;

procedure TfCentroCustos.limpaTela;
begin
    id.Text := '0';
    ccuDescricao.Text := '';
    ccuInativo.Checked := False;
    ccuReceita.Checked := False;
    ccuDespesa.Checked := False;

    ccuDescricao.SetFocus;
end;

end.

{
S� est� trazendo primeiro registro
Tela vai abrir vazia - criar procedimento para limpar
OnCreate n�o vai mais fazer select em tudo... vai usar pesquisa
Tela de pesquisa
Bot�o Novo
Bot�o Cancelar
Bot�o Excluir
Bot�o Gravar vai verificar se existe c�digo para inserir ou alterar
poScreenCenter
}

