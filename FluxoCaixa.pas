unit FluxoCaixa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, Vcl.Grids,
  Vcl.DBGrids, Vcl.Buttons, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls,
  Datasnap.DBClient, RLReport, RLPDFFilter, RLRichFilter, RLFilters,
  RLXLSFilter, Vcl.Imaging.jpeg;

type
  TfFluxoCaixa = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    dtInicio: TDateTimePicker;
    dtFim: TDateTimePicker;
    rgOpcao: TRadioGroup;
    SpeedButton1: TSpeedButton;
    DBGrid1: TDBGrid;
    dsTitulos: TDataSource;
    qryTitulos: TADOQuery;
    Label6: TLabel;
    titSaldo: TEdit;
    cdsTitulos: TClientDataSet;
    qryTitulosid: TAutoIncField;
    qryTitulostitTipo: TStringField;
    qryTitulostitPessoa: TIntegerField;
    qryTitulospesFantasia: TStringField;
    qryTitulostitValor: TBCDField;
    qryTitulostitDataVencimento: TDateTimeField;
    qryTitulostitValorRecPag: TBCDField;
    qryTitulostitDataRecPag: TDateTimeField;
    cdsTitulostitSaldo: TFloatField;
    cdsTitulosid: TIntegerField;
    cdsTitulostitPessoa: TIntegerField;
    cdsTitulospesFantasia: TStringField;
    cdsTitulostitValor: TFloatField;
    cdsTitulostitDataVencimento: TDateTimeField;
    cdsTitulostitValorRecPag: TFloatField;
    cdsTitulostitTipo: TStringField;
    cdsTitulostitSaldoPrevisto: TFloatField;
    cdsTitulostitDataRecPag: TDateTimeField;
    RLReport1: TRLReport;
    RLBand1: TRLBand;
    RLSystemInfo1: TRLSystemInfo;
    RLBand2: TRLBand;
    dbId: TRLDBText;
    dbPessoa: TRLDBText;
    dbNomeFantasia: TRLDBText;
    dbValor: TRLDBText;
    dbVencto: TRLDBText;
    dbValorPagRec: TRLDBText;
    dbDataPagRec: TRLDBText;
    RLBand3: TRLBand;
    RLImage1: TRLImage;
    lbtitulo: TRLLabel;
    RLSystemInfo2: TRLSystemInfo;
    RLSystemInfo3: TRLSystemInfo;
    RLLabel1: TRLLabel;
    rlId: TRLLabel;
    rlPessoa: TRLLabel;
    rlFantasia: TRLLabel;
    rlValor: TRLLabel;
    rlVencto: TRLLabel;
    rlValorPagRec: TRLLabel;
    rlDataPagRec: TRLLabel;
    RLXLSFilter1: TRLXLSFilter;
    RLRichFilter1: TRLRichFilter;
    RLPDFFilter1: TRLPDFFilter;
    btImprimir: TButton;
    RLDBText1: TRLDBText;
    RLLabel2: TRLLabel;
    rlSaldoPrevisto: TRLLabel;
    rlSaldoRealizado: TRLLabel;
    dbSaldoRealizado: TRLDBText;
    dbSaldoPrevisto: TRLDBText;
    rlPeriodo: TRLLabel;
    lbSaldoInicial: TRLLabel;
    procedure FormCreate(Sender: TObject);
    procedure titSaldoExit(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RLBand2BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure RLReport1BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure btImprimirClick(Sender: TObject);
    procedure RLBand3BeforePrint(Sender: TObject; var PrintIt: Boolean);
  private
    { Private declarations }
    procedure listaDados(saldoInicial: Double);
  public
    { Public declarations }
  end;

var
  fFluxoCaixa: TfFluxoCaixa;

implementation

{$R *.dfm}

uses Funcoes;

procedure TfFluxoCaixa.btImprimirClick(Sender: TObject);
begin
    if cdsTitulos.RecordCount > 0 then begin
        RLReport1.PreviewModal;
    end else begin
        mensagem('Sem informa��es para imprimir.');
    end;
end;

procedure TfFluxoCaixa.FormCreate(Sender: TObject);
begin
    dtInicio.Date := Date - 60;
    dtFim.Date := Date + 60;
    titSaldo.Text := '0,00';
    cdsTitulos.CreateDataSet;
end;

procedure TfFluxoCaixa.FormShow(Sender: TObject);
begin
    rgOpcao.ItemIndex := 1;
    ListaDados(StrToFloatDef(titSaldo.Text, 0));
end;

procedure TfFluxoCaixa.listaDados(saldoInicial: Double);
var
    saldoTemp, saldoTempPrevisto: Double;
begin
    saldoTemp := 0;
    saldoTempPrevisto := 0;

    qryTitulos.Close;
    qryTitulos.SQL.Text :=
        ' select ' +
        ' tit.id, ' +
        ' case when titTipo = ''P'' then ' +
        ' 	''Pag'' ' +
        ' else ' +
        ' 	''Rec'' end titTipo, ' +
        ' titPessoa, pesFantasia, ' +
        ' titValor, ' +
        ' titDataVencimento, ' +
        ' titValorRecPag, ' +
        ' titDataRecPag ' +
        ' from tbTitulos tit ' +
        ' left outer join tbPessoas pes on pes.id = tit.titPessoa ' +
        ' where titDataVencimento between :inicio and :fim ';

    if rgOpcao.ItemIndex = 1 then begin
        qryTitulos.SQL.Text := qryTitulos.SQL.Text +
            ' and titValorRecPag > 0 ';
    end;

    qryTitulos.SQL.Text := qryTitulos.SQL.Text +
        ' order by titDataVencimento ';

    qryTitulos.Parameters.ParamByName('inicio').Value := dtInicio.Date;
    qryTitulos.Parameters.ParamByName('fim').Value := dtFim.Date;

    qryTitulos.Open;

    if (cdsTitulos.Active) and (cdsTitulos.RecordCount > 0) then begin
        cdsTitulos.Close;
        cdsTitulos.CreateDataSet;
    end;

    if qryTitulos.RecordCount > 0 then begin
        qryTitulos.First;
        saldoTemp := saldoInicial;
        saldoTempPrevisto := saldoInicial;

        while not qryTitulos.Eof do begin
            cdsTitulos.Append;
            cdsTitulosid.Value := qryTitulosid.AsInteger;
            cdsTitulostitTipo.Value := qryTitulostitTipo.Value;
            cdsTitulostitPessoa.Value := qryTitulostitPessoa.AsInteger;
            cdsTitulospesFantasia.Value := Trim(qryTitulospesFantasia.AsString);
            cdsTitulostitValor.Value := qryTitulostitValor.AsFloat;
            cdsTitulostitDataVencimento.Value := qryTitulostitDataVencimento.AsDateTime;

            if Trim(cdsTitulostitTipo.AsString) = 'Pag' then begin
                cdsTitulostitSaldo.Value := saldoTemp - qryTitulostitValorRecPag.AsFloat;
                saldoTemp := saldoTemp - qryTitulostitValorRecPag.AsFloat;

                if qryTitulostitValorRecPag.AsFloat > 0 then begin
                    cdsTitulostitSaldoPrevisto.Value := saldoTempPrevisto - qryTitulostitValorRecPag.AsFloat;
                    saldoTempPrevisto := saldoTempPrevisto - qryTitulostitValorRecPag.AsFloat;
                end else begin
                    cdsTitulostitSaldoPrevisto.Value := saldoTempPrevisto - qryTitulostitValor.AsFloat;
                    saldoTempPrevisto := saldoTempPrevisto - qryTitulostitValor.AsFloat;
                end;
            end else begin
                cdsTitulostitSaldo.Value := saldoTemp + qryTitulostitValorRecPag.AsFloat;
                saldoTemp := saldoTemp + qryTitulostitValorRecPag.AsFloat;

                if cdsTitulostitValorRecPag.AsFloat > 0 then begin
                    cdsTitulostitSaldoPrevisto.Value := saldoTempPrevisto + qryTitulostitValorRecPag.AsFloat;
                    saldoTempPrevisto := saldoTempPrevisto + qryTitulostitValorRecPag.AsFloat;
                end else begin
                    cdsTitulostitSaldoPrevisto.Value := saldoTempPrevisto + qryTitulostitValor.AsFloat;
                    saldoTempPrevisto := saldoTempPrevisto + qryTitulostitValor.AsFloat;
                end;
            end;

            if qryTitulostitValorRecPag.AsFloat > 0 then begin
                cdsTitulostitValorRecPag.Value := qryTitulostitValorRecPag.AsFloat;
                cdsTitulostitDataRecPag.Value := qryTitulostitDataRecPag.AsDateTime;
            end;

            cdsTitulos.Post;

            qryTitulos.Next;
        end;

        DBGrid1.Columns[procuraColuna(DBGrid1, 'titSaldoPrevisto')].Visible := rgOpcao.ItemIndex = 0;

    end else begin
        mensagem('Sem t�tulos a serem listados;');
    end;
end;

procedure TfFluxoCaixa.RLBand2BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
    if RLBand2.Color = $00EEEEEE then begin
        RLBand2.Color := clWhite;
    end else begin
        RLBand2.Color := $00EEEEEE;
    end;

    dbValorPagRec.Visible := (cdsTitulos.FieldByName('titValorRecPag').AsFloat > 0);
    dbDataPagRec.Visible := (cdsTitulos.FieldByName('titValorRecPag').AsFloat > 0);
end;

procedure TfFluxoCaixa.RLBand3BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
    rlPeriodo.Caption := 'Per�odo: ' + DateToStr(dtInicio.Date) + ' at� ' + DateToStr(dtFim.date);
    lbSaldoInicial.Caption := 'Saldo Inicial: ' + Trim(titSaldo.Text);
end;

procedure TfFluxoCaixa.RLReport1BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
    if rgOpcao.ItemIndex = 0 then begin
        lbTitulo.Caption := 'Impress�o do Fluxo de Caixa Previsto';
    end else begin
        lbTitulo.Caption := 'Impress�o do Fluxo de Caixa Realizado';
    end;

    rlSaldoPrevisto.Visible := rgOpcao.ItemIndex = 0;
    dbSaldoPrevisto.Visible := rgOpcao.ItemIndex = 0;
end;

procedure TfFluxoCaixa.SpeedButton1Click(Sender: TObject);
begin
    listaDados(StrToFloatDef(titSaldo.Text, 0));
end;

procedure TfFluxoCaixa.titSaldoExit(Sender: TObject);
begin
    titSaldo.Text := FormatFloat('##0.00', StrToFloatDef(titSaldo.Text, 0));
end;

end.
