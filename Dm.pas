unit Dm;

interface

uses
  System.SysUtils, System.Classes, Data.DB, Data.Win.ADODB, Datasnap.DBClient,
  Dialogs, forms;

type
  TfDm = class(TDataModule)
    Conexao: TADOConnection;
    qryAuxiliar: TADOQuery;
    cdsConexao: TClientDataSet;
    cdsConexaoserver: TStringField;
    cdsConexaobanco: TStringField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    conectado: boolean;
  end;

var
  fDm: TfDm;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TfDm.DataModuleCreate(Sender: TObject);
var
    servidor, database: string;
begin
    servidor := '';
    database := '';
    conectado := False;

    if FileExists(ExtractFilePath(Application.Name) + 'parametros.xml') then begin
        cdsConexao.LoadFromFile(ExtractFilePath(Application.Name) + 'parametros.xml');
        cdsConexao.Open;

        servidor := cdsConexaoserver.AsString;
        database := cdsConexaobanco.AsString;
    end;

    if (servidor = '') or (database = '') then begin
        if not InputQuery('Informe', 'Nome do Servidor:', servidor) then begin
            abort;
        end;

        if not InputQuery('Informe', 'Nome do Banco de Dados:', database) then begin
            abort;
        end;
    end;

    if (servidor = '') or (database = '') then begin
        showmessage('� necess�rio informar o servidor e o banco de dados.');
        abort;
    end;

    conexao.Connected := False;
    conexao.ConnectionString := conexao.ConnectionString +
        ';datasource=' + servidor +
        ';initial catalog=' + database;

    try
        conexao.Connected := True;
        conectado := True;

        if not cdsConexao.Active then
            cdsConexao.CreateDataSet;

         cdsConexao.Edit;
         cdsConexaoserver.Value := servidor;
         cdsConexaobanco.Value := database;
         cdsConexao.Post;

         cdsConexao.SaveToFile(ExtractFilePath(Application.Name) + 'parametros.xml');
    except
        on e: exception do begin
            showmessage('Erro ao conectar:' + #13 + e.Message);
            Application.Terminate;
        end;
    end;

    qryAuxiliar.Close;
    qryAuxiliar.SQL.Text :=
        ' select isnull(parNomeEmpresa, '''') parNomeEmpresa from tbParametros ';
    qryAuxiliar.Open;

    if Trim(qryAuxiliar.FieldByName('parNomeEmpresa').AsString) = '' then begin
        qryAuxiliar.Close;
        qryAuxiliar.SQL.Text :=
            ' insert tbParametros( ' +
            ' parNomeEmpresa, parUsaCCustoRec, parUsaCCustoPag ' +
            ' ) values ( ' +
            ' ''NOVA EMPRESA TESTE'', ''N'', ''S'' )';
        qryAuxiliar.ExecSQL;
    end;
 end;

end.
