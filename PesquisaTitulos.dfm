object fPesquisaTitulos: TfPesquisaTitulos
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Pesquisa de T'#237'tulos'
  ClientHeight = 388
  ClientWidth = 908
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  DesignSize = (
    908
    388)
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 8
    Top = 8
    Width = 892
    Height = 370
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = dsTitulos
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ParentFont = False
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    OnDblClick = DBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'id'
        Title.Alignment = taRightJustify
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titTipo'
        Title.Caption = 'Tipo'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titDataCriacao'
        Title.Caption = 'Data'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titPessoa'
        Title.Alignment = taRightJustify
        Title.Caption = 'Pessoa'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'pesFantasia'
        Title.Caption = 'Fantasia'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titCentroCusto'
        Title.Alignment = taRightJustify
        Title.Caption = 'C.Custo'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ccuDescricao'
        Title.Caption = 'Descri'#231#227'o'
        Width = 150
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'titValor'
        Title.Alignment = taRightJustify
        Title.Caption = 'Valor'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titDataVencimento'
        Title.Caption = 'Vencimento'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titValorRecPag'
        Title.Alignment = taRightJustify
        Title.Caption = 'Vr Pag/Rec'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titDataRecPag'
        Title.Caption = 'Dt Pag/Rec'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titObservacao'
        Title.Caption = 'Observa'#231#227'o'
        Width = 300
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'titReferencia'
        Title.Caption = 'Refer'#234'ncia'
        Width = 90
        Visible = True
      end>
  end
  object qryTitulos: TADOQuery
    Connection = fDm.Conexao
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select '
      'tit.id, '
      'case when titTipo = '#39'P'#39' then '
      #9#39'Pag'#39
      'else '
      #9#39'Rec'#39' end titTipo, '
      'titDataCriacao, '
      'titPessoa, '
      'pesFantasia,'
      'titCentroCusto, '
      'ccuDescricao,'
      'titReferencia, '
      'format(titValor, '#39'C'#39', '#39'pt-br'#39') titValor, '
      'titDataVencimento, '
      'titValorRecPag, '
      'titDataRecPag, '
      'titObservacao'
      'from tbTitulos tit'
      'left outer join tbPessoas pes on pes.id = tit.titPessoa'
      
        'left outer join tbCentroCustos ccu on ccu.id = tit.titCentroCust' +
        'o'
      '')
    Left = 368
    Top = 144
  end
  object dsTitulos: TDataSource
    DataSet = qryTitulos
    Left = 368
    Top = 200
  end
end
