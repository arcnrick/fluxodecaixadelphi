unit Relatorios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls, Vcl.Buttons, RLReport,
  Vcl.Imaging.jpeg, RLXLSFilter, RLRichFilter, RLFilters, RLPDFFilter;

type
  TfRelatorios = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    SpeedButton1: TSpeedButton;
    Label3: TLabel;
    sbPessoa: TSpeedButton;
    Label4: TLabel;
    sbCentro: TSpeedButton;
    dtInicio: TDateTimePicker;
    dtFim: TDateTimePicker;
    titPessoa: TEdit;
    titPessoaDesc: TEdit;
    titCentroCusto: TEdit;
    titCentroCustoDesc: TEdit;
    rgOpcao: TRadioGroup;
    DBGrid1: TDBGrid;
    dsTitulos: TDataSource;
    qryTitulos: TADOQuery;
    qryAuxiliar: TADOQuery;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    btImprimir: TButton;
    RLReport1: TRLReport;
    RLBand1: TRLBand;
    RLBand2: TRLBand;
    RLBand3: TRLBand;
    RLImage1: TRLImage;
    lbTitulo: TRLLabel;
    RLSystemInfo2: TRLSystemInfo;
    RLSystemInfo3: TRLSystemInfo;
    RLLabel1: TRLLabel;
    RLSystemInfo1: TRLSystemInfo;
    dbId: TRLDBText;
    rlId: TRLLabel;
    rlPessoa: TRLLabel;
    rlFantasia: TRLLabel;
    rlCCusto: TRLLabel;
    rlDescricaoCC: TRLLabel;
    rlValor: TRLLabel;
    rlVencto: TRLLabel;
    rlValorPagRec: TRLLabel;
    rlDataPagRec: TRLLabel;
    dbPessoa: TRLDBText;
    dbNomeFantasia: TRLDBText;
    dbCCusto: TRLDBText;
    dbDescricaoCC: TRLDBText;
    dbValor: TRLDBText;
    dbVencto: TRLDBText;
    dbValorPagRec: TRLDBText;
    dbDataPagRec: TRLDBText;
    RLPDFFilter1: TRLPDFFilter;
    RLRichFilter1: TRLRichFilter;
    RLXLSFilter1: TRLXLSFilter;
    qryBusca: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure sbPessoaClick(Sender: TObject);
    procedure sbCentroClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure btImprimirClick(Sender: TObject);
    procedure RLBand2BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure RLReport1BeforePrint(Sender: TObject; var PrintIt: Boolean);
  private
    { Private declarations }
    usaCCPag, usaCCRec: Boolean;


    procedure ListaDados;
  public
    { Public declarations }
    pagOuRec: string;
  end;

var
  fRelatorios: TfRelatorios;

implementation

{$R *.dfm}

uses PesquisaCentroCustos, PesquisaPessoas, Funcoes;

procedure TfRelatorios.btImprimirClick(Sender: TObject);
begin
    if qryTitulos.RecordCount > 0 then begin
        RLReport1.PreviewModal;
    end else begin
        mensagem('Sem informa��es para imprimir.');
    end;
end;

procedure TfRelatorios.FormCreate(Sender: TObject);
begin
    dtInicio.Date := Date - 30;
    dtFim.Date := Date + 60;
    pagOuRec := 'P';

    qryBusca.Close;
    qryBusca.Sql.Text :=
        ' select ' +
        ' parUsaCCustoPag, parUsaCCustoRec ' +
        ' from tbParametros ';
    qryBusca.open;

    usaCCPag := Trim(qryBusca.FieldByName('parUsaCCustoPag').AsString) = 'S';
    usaCCRec := Trim(qryBusca.FieldByName('parUsaCCustoRec').AsString) = 'S';
end;

procedure TfRelatorios.FormShow(Sender: TObject);
begin
    rgOpcao.ItemIndex := 1;
    ListaDados;
end;

procedure TfRelatorios.ListaDados;
begin
    qryTitulos.Close;
    qryTitulos.SQL.Text :=
        ' select ' +
        ' tit.id, ' +
        ' titPessoa, pesFantasia, ' +
        ' titCentroCusto, ccuDescricao, ' +
        ' titValor, ' +
        ' titDataVencimento, ' +
        ' titValorRecPag, ' +
        ' titDataRecPag ' +
        ' from tbTitulos tit ' +
        ' left outer join tbPessoas pes on pes.id = tit.titPessoa ' +
        ' left outer join tbCentroCustos ccu on ccu.id = tit.titCentroCusto ' +
        ' where titDataVencimento between :inicio and :fim ';

    if pagOuRec = 'P' then begin
        qryTitulos.SQL.Text := qryTitulos.SQL.Text +
            ' and titTipo = ''P'' ';
    end else begin
        qryTitulos.SQL.Text := qryTitulos.SQL.Text +
            ' and titTipo = ''R'' ';
    end;

    if Trim(titPessoa.Text) <> '' then begin
        qryTitulos.SQL.Text := qryTitulos.SQL.Text +
            ' and titPessoa = :titPessoa ';
        qryTitulos.Parameters.ParamByName('titPessoa').Value := Trim(titPessoa.Text);
    end;

    if Trim(titCentroCusto.Text) <> '' then begin
        qryTitulos.SQL.Text := qryTitulos.SQL.Text +
            ' and titCentroCusto = :titCentroCusto ';
        qryTitulos.Parameters.ParamByName('titCentroCusto').Value := Trim(titCentroCusto.Text);
    end;

    if rgOpcao.ItemIndex = 1 then begin
        qryTitulos.SQL.Text := qryTitulos.SQL.Text +
            ' and titValorRecPag = 0 ';
    end else if rgOpcao.ItemIndex = 2 then begin
        qryTitulos.SQL.Text := qryTitulos.SQL.Text +
            ' and titValorRecPag > 0 ';
    end;

    qryTitulos.SQL.Text := qryTitulos.SQL.Text +
        ' order by titDataCriacao desc ';

    qryTitulos.Parameters.ParamByName('inicio').Value := dtInicio.Date;
    qryTitulos.Parameters.ParamByName('fim').Value := dtFim.Date;

    qryTitulos.Open;

    TBCDField(qryTitulos.FieldByName('titValor')).Currency := True;
    TBCDField(qryTitulos.FieldByName('titValorRecPag')).Currency := True;

    DBGrid1.Columns[procuraColuna(DBGrid1, 'titValorRecPag')].Visible := rgOpcao.ItemIndex <> 1;
    DBGrid1.Columns[procuraColuna(DBGrid1, 'titDataRecPag')].Visible := rgOpcao.ItemIndex <> 1;
end;

procedure TfRelatorios.RLBand2BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
    if RLBand2.Color = $00EEEEEE then begin
        RLBand2.Color := clWhite;
    end else begin
        RLBand2.Color := $00EEEEEE;
    end;

    dbValorPagRec.Visible := (rgOpcao.ItemIndex <> 1) and (qryTitulos.FieldByName('titValorRecPag').AsFloat > 0);
end;

procedure TfRelatorios.RLReport1BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
    rlValorPagRec.Visible := rgOpcao.ItemIndex <> 1;
    rlDataPagRec.Visible := rgOpcao.ItemIndex <> 1;
    dbValorPagRec.Visible := rgOpcao.ItemIndex <> 1;
    dbDataPagRec.Visible := rgOpcao.ItemIndex <> 1;

    if pagOuRec = 'P' then begin
        lbTitulo.Caption := 'Relat�rio de T�tulos a Pagar';
    end else begin
        lbTitulo.Caption := 'Relat�rio de T�tulos a Receber';
    end;

    rlCCusto.Visible := ((pagOuRec = 'P') and (usaCCPag)) or ((pagOuRec = 'R') and (usaCCRec));
    rlDescricaoCC.Visible := ((pagOuRec = 'P') and (usaCCPag)) or ((pagOuRec = 'R') and (usaCCRec));
    dbCCusto.Visible := ((pagOuRec = 'P') and (usaCCPag)) or ((pagOuRec = 'R') and (usaCCRec));
    dbDescricaoCC.Visible := ((pagOuRec = 'P') and (usaCCPag)) or ((pagOuRec = 'R') and (usaCCRec));
end;

procedure TfRelatorios.sbCentroClick(Sender: TObject);
begin
    try
        fPesquisaCentroCustos := TfPesquisaCentroCustos.Create(Self);
        fPesquisaCentroCustos.mostraInativo := False;

        if pagOuRec = 'P' then begin
            fPesquisaCentroCustos.titTipo := 0;
        end else begin
            fPesquisaCentroCustos.titTipo := 1;
        end;

        fPesquisaCentroCustos.ShowModal;

        if fPesquisaCentroCustos.Tag = 1 then begin
            titCentroCusto.Text := Trim(fPesquisaCentroCustos.qryCentroCusto.FieldByName('id').AsString);
            titCentroCustoDesc.Text := Trim(fPesquisaCentroCustos.qryCentroCusto.FieldByName('ccuDescricao').AsString);
        end;
    finally
        FreeAndNil(fPesquisaPessoas);
    end;
end;

procedure TfRelatorios.sbPessoaClick(Sender: TObject);
begin
    try
        fPesquisaPessoas := TfPesquisaPessoas.Create(Self);
        fPesquisaPessoas.mostraInativo := False;
        fPesquisaPessoas.ShowModal;

        if fPesquisaPessoas.Tag = 1 then begin
            titPessoa.Text := Trim(fPesquisaPessoas.qryPessoa.FieldByName('id').AsString);
            titPessoaDesc.Text := Trim(fPesquisaPessoas.qryPessoa.FieldByName('pesfantasia').AsString);
        end;
    finally
        FreeAndNil(fPesquisaPessoas);
    end;
end;

procedure TfRelatorios.SpeedButton1Click(Sender: TObject);
begin
    ListaDados;
end;

procedure TfRelatorios.SpeedButton2Click(Sender: TObject);
begin
    titPessoa.Clear;
    titPessoaDesc.Clear;
end;

procedure TfRelatorios.SpeedButton3Click(Sender: TObject);
begin
    titCentroCusto.Clear;
    titCentroCustoDesc.Clear;
end;

end.
