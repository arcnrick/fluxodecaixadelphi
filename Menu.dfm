object fMenu: TfMenu
  Left = 0
  Top = 0
  Caption = 'Fluxo de Caixa'
  ClientHeight = 476
  ClientWidth = 707
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object MainMenu1: TMainMenu
    Left = 344
    Top = 240
    object Cadastros1: TMenuItem
      Caption = 'Cadastros'
      object Parmetros1: TMenuItem
        Caption = 'Par'#226'metros'
        OnClick = Parmetros1Click
      end
      object Pessoas1: TMenuItem
        Caption = 'Pessoas'
        OnClick = Pessoas1Click
      end
      object CentrodeCustos1: TMenuItem
        Caption = 'Centro de Custos'
        OnClick = CentrodeCustos1Click
      end
    end
    object Movimentaes1: TMenuItem
      Caption = 'Movimenta'#231#245'es'
      object tulos1: TMenuItem
        Caption = 'T'#237'tulos'
        OnClick = tulos1Click
      end
      object BaixadeTtulos1: TMenuItem
        Caption = 'Baixa de T'#237'tulos'
        OnClick = BaixadeTtulos1Click
      end
    end
    object Relatrios1: TMenuItem
      Caption = 'Relat'#243'rios'
      object tulosaReceber1: TMenuItem
        Caption = 'T'#237'tulos a Receber'
        OnClick = tulosaReceber1Click
      end
      object tulosaPagar1: TMenuItem
        Caption = 'T'#237'tulos a Pagar'
        OnClick = tulosaPagar1Click
      end
      object FluxodeCaixa1: TMenuItem
        Caption = 'Fluxo de Caixa'
        OnClick = FluxodeCaixa1Click
      end
    end
    object Sair1: TMenuItem
      Caption = 'Sair'
      OnClick = Sair1Click
    end
  end
end
