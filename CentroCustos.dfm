object fCentroCustos: TfCentroCustos
  Left = 732
  Top = 329
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Centro de Custos'
  ClientHeight = 222
  ClientWidth = 439
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 22
    Top = 101
    Width = 50
    Height = 13
    Caption = 'Descri'#231#227'o:'
  end
  object Label2: TLabel
    Left = 22
    Top = 53
    Width = 37
    Height = 13
    Caption = 'C'#243'digo:'
  end
  object ccuDescricao: TEdit
    Left = 22
    Top = 120
    Width = 395
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 60
    TabOrder = 2
  end
  object ccuInativo: TCheckBox
    Left = 118
    Top = 75
    Width = 57
    Height = 17
    Caption = 'Inativo'
    TabOrder = 1
  end
  object ccuDespesa: TCheckBox
    Left = 22
    Top = 184
    Width = 160
    Height = 17
    Caption = 'Centro de Custo de Despesa'
    TabOrder = 4
  end
  object ccuReceita: TCheckBox
    Left = 22
    Top = 160
    Width = 154
    Height = 17
    Caption = 'Centro de Custo de Receita'
    TabOrder = 3
  end
  object id: TEdit
    Left = 22
    Top = 72
    Width = 60
    Height = 21
    CharCase = ecUpperCase
    Color = 15658734
    MaxLength = 30
    NumbersOnly = True
    ReadOnly = True
    TabOrder = 0
  end
  object btLimpar: TButton
    Left = 342
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Limpar'
    TabOrder = 5
    OnClick = btLimparClick
  end
  object btExcluir: TButton
    Left = 262
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Excluir'
    TabOrder = 6
    OnClick = btExcluirClick
  end
  object btCancelar: TButton
    Left = 182
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 7
    OnClick = btCancelarClick
  end
  object btPesquisar: TButton
    Left = 102
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Pesquisar'
    TabOrder = 8
    OnClick = btPesquisarClick
  end
  object btGravar_: TButton
    Left = 22
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Gravar'
    TabOrder = 9
    OnClick = btGravar_Click
  end
  object qryCentroCusto: TADOQuery
    Connection = fDm.Conexao
    Parameters = <>
    Left = 384
    Top = 48
  end
end
