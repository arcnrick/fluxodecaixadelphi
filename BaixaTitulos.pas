unit BaixaTitulos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Grids, Vcl.DBGrids, Data.Win.ADODB, Vcl.Menus, Vcl.ComCtrls, Vcl.Buttons;

type
  TfBaixaTitulos = class(TForm)
    DBGrid1: TDBGrid;
    rgTipo: TRadioGroup;
    qryTitulos: TADOQuery;
    dsTitulos: TDataSource;
    PopupMenu1: TPopupMenu;
    AbrirCadastro: TMenuItem;
    qryAuxiliar: TADOQuery;
    dtInicio: TDateTimePicker;
    dtFim: TDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    SpeedButton1: TSpeedButton;
    Label3: TLabel;
    titPessoa: TEdit;
    titPessoaDesc: TEdit;
    sbPessoa: TSpeedButton;
    Label4: TLabel;
    sbCentro: TSpeedButton;
    titCentroCusto: TEdit;
    titCentroCustoDesc: TEdit;
    cbSomenteEmAberto: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure AbrirCadastroClick(Sender: TObject);
    procedure rgTipoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure sbPessoaClick(Sender: TObject);
    procedure sbCentroClick(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
    colunaSeleciona: Boolean;
  public
    { Public declarations }
  end;

var
  fBaixaTitulos: TfBaixaTitulos;

implementation

{$R *.dfm}

uses Dm, GravarBaixa, PesquisaPessoas, PesquisaCentroCustos, Funcoes, Titulos;

procedure TfBaixaTitulos.AbrirCadastroClick(Sender: TObject);
begin
    try
        fTitulos := TfTitulos.Create(Self);
        fTitulos.codigoDaBaixa := qryTitulos.FieldByName('id').AsInteger;
        fTitulos.ShowModal;
    finally
        FreeAndNil(fTitulos);
    end;
end;

procedure TfBaixaTitulos.DBGrid1CellClick(Column: TColumn);
begin
    if not colunaSeleciona then
        Exit;

    if DBGrid1.SelectedField.FieldName = 'Baixar' then begin
        try
            if qryTitulos.FieldByName('titValorRecpag').AsFloat > 0 then begin
                alerta('T�tulo j� baixado');
                Abort;
            end;

            fGravarBaixa := TfGravarBaixa.Create(Self);
            fGravarBaixa.titValor.Text := FormatFloat('##.00', qryTitulos.FieldByName('titValor').AsFloat);
            fGravarBaixa.titValorExit(Self);
            fGravarBaixa.titDataVencimento.Text := qryTitulos.FieldByName('titDataVencimento').AsString;
            fGravarBaixa.titValorRecPag.Text := FormatFloat('##.00', qryTitulos.FieldByName('titValorRecPag').AsFloat);
            fGravarBaixa.titValorRecPagExit(Self);
            fGravarBaixa.titDataRecPag.Text := qryTitulos.FieldByName('titDataRecPag').AsString;
            fGravarBaixa.ShowModal;

            if fGravarBaixa.Tag = 1 then begin
                if (StrToFloatDef(fGravarBaixa.titValorRecPag.Text, 0) = 0) or
                (Trim(fGravarBaixa.titDataRecPag.Text) = '/  /') then begin
                    alerta('A data da baixa informada ou o valor a ser baixado est�o incorretos.');
                    abort;
                end;


                qryAuxiliar.Close;
                qryAuxiliar.SQL.Text :=
                    ' update tbTitulos ' +
                    ' set titValorRecPag = :titValorRecPag ' +
                    ' , titDataRecPag = :titDataRecPag ' +
                    ' where id = :id ';
                qryAuxiliar.Parameters.ParamByName('titValorRecPag').Value := StrToFloatDef(fGravarBaixa.titValorRecPag.Text, 0);
                qryAuxiliar.Parameters.ParamByName('titDataRecPag').Value := Trim(fGravarBaixa.titDataRecPag.Text);
                qryAuxiliar.Parameters.ParamByName('id').Value := qryTitulos.FieldByName('id').AsInteger;
                qryAuxiliar.ExecSQL;

                Self.FormShow(Self);
            end;
        finally
            FreeAndNil(fGravarBaixa);
        end;

        DBGrid1.SelectedIndex := DBGrid1.SelectedIndex;
    end;
end;

procedure TfBaixaTitulos.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
    button: integer;
    R: TRect;
    bcolor: TColor;
begin
    if Column.FieldName = 'Baixar' then begin
        DBGrid1.Canvas.FillRect(Rect);
        BUTTON := 0;
        R:=Rect;
        InflateRect(R,-1,-1);

        DrawFrameControl(DBGrid1.Canvas.Handle,R,BUTTON, BUTTON or BUTTON);
        bcolor := DBGrid1.Canvas.Brush.Color;
        DBGrid1.Canvas.Brush.Color := clBtnFace;

        DrawText(DBGrid1.Canvas.Handle,'Baixar',7,R,DT_VCENTER or DT_CENTER);
        DBGrid1.Canvas.Brush.Color := bcolor;
    end;
end;

procedure TfBaixaTitulos.DBGrid1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
    colunaSeleciona := False;

    colunaSeleciona := ((x > 1) and ((x <= DBGrid1.Columns[0].Width + 10)));
end;

procedure TfBaixaTitulos.FormCreate(Sender: TObject);
begin
    dtInicio.Date := Date;
    dtFim.Date := Date + 30;
    colunaSeleciona := False;
end;

procedure TfBaixaTitulos.FormShow(Sender: TObject);
begin
    qryTitulos.Close;
    qryTitulos.SQL.Text :=
        ' select ' +
        ' '''' Baixar, ' +
        ' tit.id, ' +
        ' case when titTipo = ''P'' then ' +
        ' 	''Pag'' ' +
        ' else ' +
        ' 	''Rec'' end titTipo, ' +
        ' titDataCriacao, titPessoa, pesFantasia, ' +
        ' titCentroCusto, ccuDescricao, titReferencia, ' +
        ' titValor, ' +
        ' titDataVencimento, ' +
        ' titValorRecPag, ' +
        ' titDataRecPag, titObservacao ' +
        ' from tbTitulos tit ' +
        ' left outer join tbPessoas pes on pes.id = tit.titPessoa ' +
        ' left outer join tbCentroCustos ccu on ccu.id = tit.titCentroCusto ' +
        ' where titDataVencimento between :inicio and :fim ';

    if rgTipo.ItemIndex = 1 then begin
        qryTitulos.SQL.Text := qryTitulos.SQL.Text +
            ' and titTipo = ''P'' ';
    end else if rgTipo.ItemIndex = 2 then begin
        qryTitulos.SQL.Text := qryTitulos.SQL.Text +
            ' and titTipo = ''R'' ';
    end;

    if Trim(titPessoa.Text) <> '' then begin
        qryTitulos.SQL.Text := qryTitulos.SQL.Text +
            ' and titPessoa = :titPessoa ';
        qryTitulos.Parameters.ParamByName('titPessoa').Value := Trim(titPessoa.Text);
    end;

    if Trim(titCentroCusto.Text) <> '' then begin
        qryTitulos.SQL.Text := qryTitulos.SQL.Text +
            ' and titCentroCusto = :titCentroCusto ';
        qryTitulos.Parameters.ParamByName('titCentroCusto').Value := Trim(titCentroCusto.Text);
    end;

    if cbSomenteEmAberto.Checked then begin
        qryTitulos.SQL.Text := qryTitulos.SQL.Text +
            ' and titValorRecPag = 0 ';
    end;

    qryTitulos.SQL.Text := qryTitulos.SQL.Text +
        ' order by titDataCriacao desc ';

    qryTitulos.Parameters.ParamByName('inicio').Value := dtInicio.Date;
    qryTitulos.Parameters.ParamByName('fim').Value := dtFim.Date;

    qryTitulos.Open;

    TBCDField(qryTitulos.FieldByName('titValor')).Currency := True;
    TBCDField(qryTitulos.FieldByName('titValorRecPag')).Currency := True;
end;

procedure TfBaixaTitulos.rgTipoClick(Sender: TObject);
begin
    Self.FormShow(Sender);
end;

procedure TfBaixaTitulos.sbCentroClick(Sender: TObject);
begin
    try
        fPesquisaCentroCustos := TfPesquisaCentroCustos.Create(Self);
        fPesquisaCentroCustos.mostraInativo := False;

        if rgTipo.ItemIndex = 1 then begin
            fPesquisaCentroCustos.titTipo := 0;
        end else if rgTipo.ItemIndex = 2 then begin
            fPesquisaCentroCustos.titTipo := 1;
        end;

        fPesquisaCentroCustos.ShowModal;

        if fPesquisaCentroCustos.Tag = 1 then begin
            titCentroCusto.Text := Trim(fPesquisaCentroCustos.qryCentroCusto.FieldByName('id').AsString);
            titCentroCustoDesc.Text := Trim(fPesquisaCentroCustos.qryCentroCusto.FieldByName('ccuDescricao').AsString);
        end;
    finally
        FreeAndNil(fPesquisaPessoas);
    end;
end;

procedure TfBaixaTitulos.sbPessoaClick(Sender: TObject);
begin
    try
        fPesquisaPessoas := TfPesquisaPessoas.Create(Self);
        fPesquisaPessoas.mostraInativo := False;
        fPesquisaPessoas.ShowModal;

        if fPesquisaPessoas.Tag = 1 then begin
            titPessoa.Text := Trim(fPesquisaPessoas.qryPessoa.FieldByName('id').AsString);
            titPessoaDesc.Text := Trim(fPesquisaPessoas.qryPessoa.FieldByName('pesfantasia').AsString);
        end;
    finally
        FreeAndNil(fPesquisaPessoas);
    end;
end;

procedure TfBaixaTitulos.SpeedButton1Click(Sender: TObject);
begin
    Self.FormShow(Sender);
end;

end.
